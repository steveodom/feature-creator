trading-features-creator expects quotes oldest to freshest.

Calculates features based on a set of quotes and a recipe.

Used by:
features/trading-model-creator
features/writer
forecasts/daily
forecasts/realtime
utilities/quote-endpoint
