const validateChange = (recipe = []) => {
  let isValid = true;
  let msg = [];

  const method = recipe.hasOwnProperty('method');
  if (!method) {
    msg.push('A calculation method is required');
  };

  const n = recipe.hasOwnProperty('n');
  if (!n) {
    msg.push('A period n is require to look foward');
  };

  const threshold = recipe.hasOwnProperty('threshold1');
  if (!threshold) {
    msg.push('A threshold1 is required to know how much to measure');
  };

  isValid = method && n && threshold;
  return {isValid, msg};
}

export default validateChange;
