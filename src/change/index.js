import ChangeMethods from './methods';

export default class Change {
  constructor(series,recipe) {
    this.series = series;
    this.recipe = recipe;
    this.changeObject = recipe.changeCalc;
  }

  init(MAaverages) {
    // we use MAaverages for its std dev calcs for each quote window.
    this.methods = new ChangeMethods(this.series, this.recipe.changeCalc.n, MAaverages);
  }

  build(quote, i) {
    const {method, threshold1, threshold2, threshold3} = this.changeObject;
    return {
      change: this.methods[method](i, threshold1, threshold2, threshold3)
    };
  }
}
