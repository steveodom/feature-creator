import { nextDayChange, nextDayChangeGeneric } from './../utils/calcs';
import _ from 'lodash';

export default class ChangeMethods {
  constructor(series, lookAhead = 1, MAaverages) {
    this.series = series;
    this.lookAhead = lookAhead;
    this.stdDevs = false;
    this.twoStdDevsAsPctMove = false;
    this.findMovingAverageTargetForStdDev(MAaverages);
  }

  findMovingAverageTargetForStdDev(MAaverages) {
    // moving average recipe must contain a 20 period calculation
    const period = 20;
    const targetAverages = MAaverages && MAaverages.find( (avg) => (avg.type === 'rolling' && avg.period === period));
    if (targetAverages && targetAverages.methods && targetAverages.methods.averages && targetAverages.methods.averages.bb) {
      this.stdDevs = targetAverages.methods.averages.bb.map( (obj) => (obj.upper - obj.middle || 0));
      this.twoStdDevsAsPctMove = targetAverages.methods.averages.bb.map( (obj) => ( nextDayChangeGeneric(obj.upper, obj.middle) || 0));
    }
  }

  change(i) {
    const start = this.series[i];
    const finish = this.series[i + this.lookAhead];
    return nextDayChange(start, finish);
  }

  isGreaterThan(i, n = 1) {
    return this.change(i) > n;
  }

  isGreaterThanStdDev(i, n = 1) {
    return this.changeAsPctOfStdDev(i) > n;
  }

  isLessThan(i, n = 1) {
    return this.change(i) < n;
  }

  isLessThanStdDev(i, n = 1) {
    return this.changeAsPctOfStdDev(i) < n;
  }

  assignRange(i, max, min) {
    const change = this.change(i);
    if (change > max) {
      return 'up';
    } else if (change < min) {
      return 'down';
    } else {
      return 'flat';
    }
  }

  assignQuad(i, high, mid, low) {
    const change = this.change(i);
    if (change > high) {
      return 'up';
    } else if (change <= low) {
      return 'down';
    } else if (change <= high && change > mid) {
      return 'up-flat';
    } else if (change <= mid && change > low) {
      return 'down-flat';
    } else {
      return 'flat';
    }
  }

  changeAsPctOfStdDev(i) {
    const change = this.change(i);
    const stdDev = this.stdDevs[i];
    return _.round(_.divide(change, stdDev), 4);
  }

  changeAsPctOfTwoStdDeviationMove(i) {
    const change = this.change(i);
    const stdDev = Math.abs(this.twoStdDevsAsPctMove[i]);
    if (stdDev === 0) return 0;
    return _.round(_.divide(change, stdDev), 4);
  }

  assignQuadDeviation(i, high, mid, low) {
    if (!this.stdDevs) return this.assignQuad(i, high, mid, low);

    const changeAsPctOfStdDev = this.changeAsPctOfTwoStdDeviationMove(i);
    if (changeAsPctOfStdDev > high) {
      return 'up';
    } else if (changeAsPctOfStdDev <= low) {
      return 'down';
    } else if (changeAsPctOfStdDev <= high && changeAsPctOfStdDev > mid) {
      return 'up-flat';
    } else if (changeAsPctOfStdDev <= mid && changeAsPctOfStdDev > low) {
      return 'down-flat';
    } else {
      return 'flat';
    }
  }
}
