import {targetArray, arrayAvg, arrayDeviation} from './../utils/calcs';

export default class Change {
  constructor(data) {
    this.data = data;
    this.closes = data.map ( (obj) => {return obj.close } );
  }

  rolling(days) {
    return this.closes.map( (num, i) => {
      return this.calcAvgForWindow(i + 1, days);
    });
  }

  calcAvgForWindow(i, days) {
    let target = targetArray(i, days, this.data);
    let closes = target.map ( (obj) => obj.close);
    let avg = arrayAvg(closes);
    let deviation = arrayDeviation(closes);

    if (avg) {
      return {avg, upper: avg + deviation, lower: avg - deviation};
    } else {
      return {avg: 0, upper: 0, lower: 0};
    }
  }
}
