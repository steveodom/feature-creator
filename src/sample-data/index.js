export const defaultRecipe = {
  basic: ['closeAtHigh', 'closeAtLow'],
  movingAverage: [
    {feature: 'movingAverage', name: 'ma5', 'period': 5, type: 'rolling'},
    {feature: 'movingAverage', name: 'ma20', 'period': 20, type: 'rolling'},
    {feature: 'closeAboveAvg', name: 'above5Avg', 'period': 5, type: 'rolling'},
    {feature: 'daysAgoChange', name: 'daysAgo5Change', 'period': 5, type: 'rolling', daysAgo: 5}
  ],
  matrixChart: [
    {feature: 'chart', name: 'mx5', 'period': 5, type: 'closes', granularity: 8, useAverageRange: false},
    {feature: 'chart', name: 'mx20', 'period': 20, type: 'volume', granularity: 8, useAverageRange: false},
    {feature: 'chart', name: 'mx20', 'period': 20, type: 'obv', granularity: 4, useAverageRange: true}
  ],
  matrixChartPost: [
    {feature: 'isFlatPattern', 'period': 5, type: 'closes'},
    {feature: 'isHighVolumeAndBelowToAbove', 'period': 5, type: 'closes'}
  ],
  supportResistance: [
    {feature: 'nearLevel', name: 'nearLevel236', 'period': 20, level: 236 },
    {feature: 'supportLevel', name: 'supportLevel236', 'period': 5, level: 236 },
    {feature: 'onBalanceVolume', type: 'obv' },
    {feature: 'obvChange', type: 'obv', 'daysAgo': 14 }
  ],
  relativeStrength: [
    {feature: 'relativePct', 'daysAgo': 20 },
    {feature: 'relativePct', 'daysAgo': 5 },
    {feature: 'rsi', 'daysAgo': 14 }
  ],
  candlesticks: [
    {feature: 'abandonedBaby', 'daysAgo': 5}
  ],
  changeCalc: {
    method: 'isGreaterThan',
    n: 1,
    threshold1: 1,
    threshold2: -0.5
  }
};

export const defaultSeries = [
  {
    Date: 20160805,
    close: 782.22,
    high: 783.04,
    low: 772.34,
    open: 773.78,
    volume: 1801200
  },
  {
    Date: 20160808,
    close: 781.76,
    high: 782.63,
    low: 778.091,
    open: 782,
    volume: 1107900
  },
  {
    Date: 20160809,
    close: 784.26,
    high: 788.94,
    low: 780.57,
    open: 781.1,
    volume: 1318900
  }
];


export const defaultQuote = [
  {
    Date: 20160805,
    close: 782.22,
    high: 783.04,
    low: 772.34,
    open: 773.78,
    volume: 1801200
  }
];

export const longerSeries = [
  {
    Date: 20160805,
    close: 782.22,
    high: 783.04,
    low: 772.34,
    open: 773.78,
    volume: 1801200
  },
  {
    Date: 20160808,
    close: 781.76,
    high: 782.63,
    low: 778.091,
    open: 782,
    volume: 1107900
  },
  {
    Date: 20160809,
    close: 784.26,
    high: 788.94,
    low: 780.57,
    open: 781.1,
    volume: 1318900
  },
  {
    Date: 20160810,
    close: 785.26,
    high: 789.94,
    low: 780.00,
    open: 784.1,
    volume: 1318900
  },
  {
    Date: 20160811,
    close: 786.26,
    high: 787.94,
    low: 783.00,
    open: 785.1,
    volume: 1318900
  }
]
