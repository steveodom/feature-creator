import _ from 'lodash';
import basic from './../basic';
import ma from './../moving-averages';
import mc from './../matrix-chart';
import sr from './../support-resistance';
import rs from './../relative-strength';
import vo from './../volatility';
import cs from './../candlesticks';

export default class RecipeGroup {
  constructor() {
    this.normalize = [];
    this.quantile = [];
    this.ngram = [];
  }

  checkMeta(group, feature) {
    const klass = feature.period || feature.daysAgo;
    const res = group.meta(feature, klass);
    if (res) {
      if (res.hasOwnProperty('normalize')) {
        const token = group.token(feature);
        if (res.normalize === 'normalize') {
          this.normalize.push(`'${token}'`);
        }

        if (res.normalize === 'quantile') {
          this.quantile.push(`'${token}'`);
        }

        if (res.normalize === 'ngram') {
          this.ngram.push(`'${token}'`);
        }
      }
    }
  }

  group(method = 'normalize') {
    return `group(${this[method].join(',')})`.replace(/\'/g, '');
  }
  
  build(recipe) {
    let basicFeatures = [...recipe.basic, 'close', 'high', 'low', 'open'];
    basicFeatures.forEach( (feature) => (this.checkMeta(basic, feature)));
    recipe.movingAverage.forEach( (feature) => (this.checkMeta(ma, feature)));
    recipe.matrixChart.forEach( (feature) => (this.checkMeta(mc, feature)));
    recipe.supportResistance.forEach( (feature) => (this.checkMeta(sr, feature)));

    if (!_.isEmpty(recipe.matrixChartPost)) {
      recipe.matrixChartPost.forEach( (feature) => (this.checkMeta(mc, feature)));
    }

    if (!_.isEmpty(recipe.relativeStrength)) {
      recipe.relativeStrength.forEach( (feature) => (this.checkMeta(rs, feature)));
    }

    if (!_.isEmpty(recipe.volatility)) {
      recipe.volatility.forEach( (feature) => (this.checkMeta(vo, feature)));
    }

    if (!_.isEmpty(recipe.candlesticks)) {
      recipe.candlesticks.forEach( (feature) => (this.checkMeta(cs, feature)));
    }
    
    return {
      normalize: this.group('normalize'), 
      quantile: this.group('quantile'),
      ngram: this.group('ngram'),
    };
  }
}

