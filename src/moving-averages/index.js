import {defaultRecipe} from './../sample-data';
import MovingAverageMethods from './methods';
import MACalcs from './calcs';
import metaDetails from './meta';

export default class MovingAverages {
  constructor(series, recipe, indexSeries) {
    this.series = series;
    this.calcs = new MACalcs(series);
    this.recipe = recipe;
    this.indexSeries = indexSeries;
  }

  init() {
    let averages;

    this.recipe.movingAverage.forEach( (item) => {
      switch (item.type) {
      case 'rolling':
        averages = this.calcs.rolling(item.period);
        break;
      case 'donchian':
        averages = this.calcs.donchian(item.period);
        break;
      case 'trueRange':
        averages = this.calcs.trueRange();
        break;
      case 'exponential':
        averages = this.calcs.exponential(item.period);
        break;
      default:
        averages = this.calcs.rolling(item.period);
      }
      item.methods = new MovingAverageMethods(this.series, averages, this.indexSeries);
    });
  }

  static token(recipeItem) {
    const baseName = 'ma';
    const typeInitial = recipeItem.type[0];
    return `${baseName}${typeInitial}-${recipeItem.feature}-${recipeItem.period}`;
  }

  static meta(recipeItem, klass) {
    const featureName = recipeItem.feature;
    try {
      return metaDetails[featureName](klass);
    } catch (err) {
      return null
    }    
  }

  build(quote, i) {
    let obj = {};

    this.recipe.movingAverage.forEach( (item) => {
      const methods = item.methods;
      
      const name = MovingAverages.token(item);
      obj[name] = methods[item.feature](quote, i, item);
    });
    return obj;
  }
}
