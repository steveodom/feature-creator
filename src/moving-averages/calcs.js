import {targetArray, arrayAvg, arrayDeviation} from './../utils/calcs';
import _ from 'lodash';
import {min, max} from 'd3-array';
import round from 'lodash/round';
import {EMA, SMA, BollingerBands as BB} from 'technicalindicators';

export default class MovingAverage {
  constructor(data) {
    this.data = data;
    this.closes = data.map ( (obj) => {return obj.close } );
    this.volumes = data.map ( (obj) => {return obj.volume } );
    this.avgTrueRanges = [];
  }

  rolling(days) {
    const input = { values: this.closes, period: days, stdDev : 2};
    const avgs = SMA.calculate(input);
    const bb = BB.calculate(input);
    const diff = this.closes.length - avgs.length;
    const pad = _.fill(Array(diff), 0);
    const padBB = _.fill(Array(diff), {middle: 0, upper: 0, lower: 0, pb: 0})
    return {
      avg: [...pad, ...avgs],
      bb: [...padBB, ...bb]
    }
  }

  donchian(days) {
    return this.data.map( (num, i) => {
      return this.calcDonchianForWindow(i + 1, days);
    });
  }

  exponential(days) {
    const input = { values: this.closes, period: days};
    const avgs = EMA.calculate(input);
    const diff = this.closes.length - avgs.length;
    const pad = _.fill(Array(diff), 0);
    return [...pad, ...avgs];
  }

  calcTrueRange(today, i) {
    const a = round(today.high - today.low, 2);
    const yesterday = this.data[i-1];
    if (!yesterday) return a;
    const b = Math.abs(today.high - yesterday.close);
    const c = Math.abs(today.low - yesterday.close);
    return round(max([a, b, c]),4);
  }

  trueRange() {
    const tr = this.data.map( (obj, i) => {
      return this.calcTrueRange(obj, i);
    });
    this.avgTrueRanges = tr;
    return tr;
  }

  // TODO: where is this being used?
  rollingTrueRange(days) {
    this.avgTrueRanges.map( (num, i) => {
      let target = targetArray(i, days, this.avgTrueRanges);
      return this.avgTrueRange(target, days);
    });
  }

  seriesDeviationOfCloses() {
    return round(arrayDeviation(this.closes), 4);
  }

  seriesDeviationOfVolume() {
    return round(arrayDeviation(this.volumes), 4);
  }

  // http://bigpicture.typepad.com/comments/files/turtlerules.pdf
  calcN(tr, pdn, days) {
    return round(( (days - 1) * pdn + tr ) / days, 4);
  }

  avgTrueRange(dailyArray, days) {
    const res = [];
    dailyArray.forEach( (tr, i) => {
      // pdn stands for previous day N.
      const pdn = i === 0 ? tr : res[i - 1];   
      const n = this.calcN(tr, pdn, days); 
      res.push(n);
    });
    return res;
  }

  calcAvgForWindow(i, days) {
    let target = targetArray(i, days, this.data);
    let closes = target.map ( (obj) => obj.close);
    let avg = arrayAvg(closes);
    let deviation = arrayDeviation(closes);

    if (avg) {
      return {avg, upper: avg + deviation, lower: avg - deviation, deviation};
    } else {
      return {avg: 0, upper: 0, lower: 0, deviation: 0};
    }
  }

  calcDonchianForWindow(i, days) {
    let target = targetArray(i, days, this.data);
    const {high, low} = this.highsAndLows(target);

    if (high && low) {
      return {high, low};
    } else {
      return null;
    }
  }

  highsAndLows(target) {
    let highs = target.map ( (obj) => {return obj.high } );
    let lows = target.map ( (obj) => {return obj.low } );

    let high = max(highs);
    let low = min(lows);
    return {high, low}
  }

  bands(days) {
    return this.data.map( (num, i) => {
      return this.calcAvgForWindow(i + 1, days);
    });
  }
}
