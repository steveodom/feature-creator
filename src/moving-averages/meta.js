const chartDetails = {
  movingAverage(klass) {
    return {
      group: 'Price Data',
      hidden: false,
      yAxisID: 'left',
      label: 'Moving Average',
      type: 'line',
      key: `${klass}-movingAverage`,
      normalize: 'normalize'
    }
  },
  closeAboveAvg(klass) {
    return {
      group: 'Price Data',
      hidden: true,
      yAxisID: 'right',
      label: 'Close Above Average',
      type: 'line',
      key: `${klass}-closeAboveAvg`,
      showLine: false,
      pointRadius: 6
    }
  },
  closeBelowAvg(klass) {
    return {
      group: 'Price Data',
      hidden: true,
      yAxisID: 'right',
      label: 'Close Below Average',
      type: 'line',
      key: `${klass}-closeBelowAvg`,
      showLine: false,
      pointRadius: 6
    }
  },
  daysAgoChange(klass) {
    return {
      group: 'Price Data',
      hidden: true,
      yAxisID: 'right',
      label: 'Days Ago Change',
      type: 'line',
      key: `${klass}-daysAgoChange`,
      pointRadius: 6,
      showLine: false,
      normalize: 'quantile'
    }
  },
  daysAgoChangeAsPctOfTwoStdDev(klass) {
    return {
      group: 'Price Data',
      hidden: true,
      yAxisID: 'right',
      label: 'daysAgoChangeAsPctOfTwoStdDev',
      type: 'line',
      key: `${klass}-daysAgoChangeAsPctOfTwoStdDev`,
      pointRadius: 6,
      showLine: false,
      normalize: 'quantile'
    }
  },
  upperDeviation(klass) {
    return {
      group: 'Price Data',
      hidden: true,
      yAxisID: 'left',
      label: 'Upper Deviation',
      type: 'line',
      key: `${klass}-upperDeviation`,
      pointRadius: 6,
      showLine: false,
      normalize: 'normalize'
    }
  },
  lowerDeviation(klass) {
    return {
      group: 'Price Data',
      hidden: true,
      yAxisID: 'left',
      label: 'Lower Deviation',
      type: 'line',
      key: `${klass}-lowerDeviation`,
      pointRadius: 6,
      showLine: false,
      normalize: 'normalize'
    }
  }
};

export default chartDetails;
