import MovingAverageMethods from './methods';
import _ from 'lodash';


const validateMovingAverage = (recipe = []) => {
  let isValid = true;
  let msg = [];
  const methods = new MovingAverageMethods({}, {});

  let ary = recipe.map( (item) => {
    const a = item.hasOwnProperty('period');

    if (!a) {
      msg.push('Period field required');
    };

    const b = item.hasOwnProperty('type');
    if (!b) {
      msg.push('Type field required');
    };

    const c = item.hasOwnProperty('feature');
    if (!c) {
      msg.push('Feature field required');
    };

    const d = item.hasOwnProperty('name');
    if (!d) {
      msg.push('Name field required');
    };

    let e = typeof methods[item.feature] === 'function'
    if (!e) {
      msg.push(`MA Specified feature was not found: ${item.feature}`);
    };

    return a && b && c && d && e;
  })

  isValid = !_.includes(ary, false);
  return {isValid, msg};
}

export default validateMovingAverage;
