import _ from 'lodash';
import { nextDayChange, nextDayChangeGeneric, withinQuoteScope } from './../utils/calcs';

export default class MovingAverageMethods {
  constructor(series, averages, indexSeriesByDate) {
    this.series = series;
    this.averages = averages;
    this.indexSeries = _.values(indexSeriesByDate);
  }

  movingAverage(quote, i) {
    return this.averages.avg[i];
  }

  closeAboveAvg(quote, i) {
    return quote.close > this.movingAverage(quote, i);
  }

  closeBelowAvg(quote, i) {
    return quote.close < this.movingAverage(quote, i);
  }

  upperDeviation(quote, i) {
    return this.averages.bb[i].upper || quote.high;
  }

  lowerDeviation(quote, i) {
    return this.averages.bb[i].lower || quote.low;
  }

  brokeUpper(quote, i) {
    return quote.high > this.upperDeviation(quote, i);
  }

  brokeLower(quote, i) {
    return quote.low < this.lowerDeviation(quote, i);
  }

  daysAgoChange(quote, i, options = {}) {
    if (!withinQuoteScope(i, options.daysAgo)) return 0;
    
    const daysAgoQuote = this.series[i - options.daysAgo];
    if (daysAgoQuote && !daysAgoQuote.close) return 0;
    return nextDayChange(daysAgoQuote, quote);
  }

  daysAgoChangeAsPctOfTwoStdDev(quote, i, options = {}) {
    const change = this.daysAgoChange(quote, i, options);
    const bb = this.averages.bb[i];
    const stdDev = nextDayChangeGeneric(bb.upper, bb.middle);
    if (stdDev === 0) return 0;
    return _.round(_.divide(change, stdDev), 4);
  }

  indexDaysAgoChange(quote, i, options = {}) {
    if (!withinQuoteScope(i, options.daysAgo)) return 0;
    const indexQuote = this.indexSeries[i];
    const daysAgoQuote = this.indexSeries[i - options.daysAgo];
    if (daysAgoQuote && !daysAgoQuote.close) return 0;
    return nextDayChange(daysAgoQuote, indexQuote);
  }
}
