import { nextDayChangeGeneric, isWithin, withinQuoteScope } from './../utils/calcs';

export default class SRMethods {
  constructor(series, averages) {
    this.series = series;
    this.averages = averages;
  }

  onBalanceVolume(quote, i) {
    return this.averages[i] || 0;
  }

  obvChange(quote, i, options) {
    const lookback = options.period || options.daysAgo || 14;
    if (!withinQuoteScope(i, lookback)) return 'flat';
    
    const daysAgoQuote = this.averages[i - lookback];
    if (!daysAgoQuote) return 'flat';
    const change = nextDayChangeGeneric(daysAgoQuote, this.averages[i]);
    const high = 0.3;
    const mid = 0;
    const low = -0.3;

    if (change > high) {
      return 'up';
    } else if (change <= low) {
      return 'down';
    } else if (change <= high && change > mid) {
      return 'up-flat';
    } else if (change <= mid && change > low) {
      return 'down-flat';
    } else {
      return 'flat';
    }
  }



  nearLevel(quote, i, options = {}) {
    const target = this.supportLevel(quote, i, options);
    return isWithin(quote.close, target, 0.002);
  }

  supportLevel(quote, i, options = {}) {
    const { level } = options;
    return this.averages[i][level];
  }


}
