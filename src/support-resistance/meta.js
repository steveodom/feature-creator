const chartDetails = {
  nearLevel(klass) {
    return {
      group: 'Price Data',
      hidden: true,
      yAxisID: 'right',
      label: 'Support Near Level',
      type: 'line',
      key: `${klass}-nearLevel`,
      showLine: false,
      pointRadius: 6
    }
  },

  supportLevel(klass) {
    return {
      group: 'Price Data',
      hidden: true,
      yAxisID: 'left',
      label: 'Support Level',
      type: 'line',
      key: `${klass}-supportLevel`,
      normalize: 'normalize'
    }
  },

  onBalanceVolume(klass) {
    return {
      group: 'Price Data',
      hidden: true,
      yAxisID: 'left',
      label: 'On Balance Volume',
      type: 'line',
      key: `${klass}-supportLevel`,
      normalize: 'normalize'
    }
  }
};

export default chartDetails;
