import _ from 'lodash';
import {targetArray, targetHighsAndLows} from './../utils/calcs';
import {padTechnicalIndicators} from './../utils/helpers';
import {OBV} from 'technicalindicators';

export default class SRCalcs {
  constructor(data) {
    this.data = data;
    this.closes = data.map ( (obj) => {return obj.close } );
    this.volumes = data.map ( (obj) => {return obj.volume } );
  }

  static obv(closes, volumes) {
    const input = { close: closes, volume: volumes};
    const avgs = OBV.calculate(input);
    const output = padTechnicalIndicators(avgs, closes.length);
    return output;
  }

  obv() {
    const input = { close: this.closes, volume: this.volumes};
    const avgs = OBV.calculate(input);
    const output = padTechnicalIndicators(avgs, this.closes.length);
    return output;
  }

  fibonacci(days) {
    return this.data.map( (num, i) => {
      return this.calcFibonacciForWindow(i + 1, days);
    });
  }

  calcFibonacci(high, low) {
    const range = high - low;
    return {
      100: range * 1 + low,
      618: range * 0.618 + low,
      50: range * 0.50 + low,
      382: range * 0.382 + low,
      236: range * 0.236 + low
    }
  }

  calcFibonacciForWindow(i, days) {
    let target = targetArray(i, days, this.data);
    const {high, low} = targetHighsAndLows(target);
    if (high && low) {
      return this.calcFibonacci(high, low);
    } else {
      return null;
    }
  }
}
