import SRMethods from './methods';
import SRCalcs from './calcs';
import metaDetails from './meta';
import _ from 'lodash';

export default class SupportResistance {
  constructor(series,recipe) {
    this.series = series;
    this.calcs = new SRCalcs(series);
    this.recipe = recipe;
  }

  init() {
    let averages;
    if (_.isEmpty(this.recipe.supportResistance)) return null;
    this.recipe.supportResistance.forEach( (item) => {
      switch (item.type) {
      case 'obv':
        averages = this.calcs.obv();
        break;
      default:
        averages = this.calcs.fibonacci(item.period);
      }
      item.methods = new SRMethods(this.series, averages);
    });
  }

  static token(recipeItem) {
    const baseName = 'sr';
    let level = recipeItem.level || 0;
    let period = recipeItem.period || 0;
    return `${baseName}-${recipeItem.feature}-${level}-${period}`;
  }

  static meta(recipeItem, klass) {
    const featureName = recipeItem.feature;
    try {
      return metaDetails[featureName](klass);
    } catch (err) {
      return null
    }    
  }

  build(quote, i) {
    let obj = {};
    if (_.isEmpty(this.recipe.supportResistance)) return obj;
    this.recipe.supportResistance.forEach( (item) => {
      const methods = item.methods;
      const name = SupportResistance.token(item);
      obj[name] = methods[item.feature](quote, i, item);
    });
    return obj;
  }
}
