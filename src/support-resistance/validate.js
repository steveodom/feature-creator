import SRMethods from './methods';
import _ from 'lodash';

const validateSupportResistance = (recipe = []) => {
  let isValid = true;
  let msg = [];
  const methods = new SRMethods({}, {});

  let ary = recipe.map( (item) => {
    // const a = item.hasOwnProperty('period');    

    const c = item.hasOwnProperty('feature');
    if (!c) {
      msg.push('Feature field required');
    }

    let e = typeof methods[item.feature] === 'function'
    if (!e) {
      msg.push(`SR Specified feature was not found: ${item.feature}`);
    }

    return c && e;
  })

  isValid = !_.includes(ary, false);
  return {isValid, msg};
}

export default validateSupportResistance;
