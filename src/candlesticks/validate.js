import CMethods from './methods';
import _ from 'lodash';

const validateCandlesticks = (recipe = []) => {
  let isValid = true;
  let msg = [];
  const methods = new CMethods({}, {});

  let ary = recipe.map( (item) => {
    const a = item.hasOwnProperty('daysAgo');

    if (!a) {
      msg.push('Period field required');
    }

    const c = item.hasOwnProperty('feature');
    if (!c) {
      msg.push('Feature field required');
    }

    let e = typeof methods[item.feature] === 'function'
    if (!e) {
      msg.push(`CS Specified feature was not found: ${item.feature}`);
    }

    return a && c && e;
  })

  isValid = !_.includes(ary, false);
  return {isValid, msg};
}

export default validateCandlesticks;
