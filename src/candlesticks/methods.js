import _ from 'lodash';
import {
  abandonedbaby,
  bearishengulfingpattern,
  bullishengulfingpattern,
  darkcloudcover,
  downsidetasukigap,
  doji,
  dragonflydoji,
  gravestonedoji,
  bullishharami,
  bearishharami,
  bearishharamicross,
  bullishharamicross,
  bullishmarubozu,
  bearishmarubozu,
  eveningdojistar,
  eveningstar,
  piercingline,
  bullishspinningtop,
  bearishspinningtop,
  morningdojistar,
  morningstar,
  threeblackcrows,
  threewhitesoldiers,
  bullish,
  bearish
} from 'technicalindicators';

export default class CandlestickMethods {
  constructor(series, inputs) {
    this.series = series;
    this.inputs = inputs;
  }

  buildInputs(i) {
    const target = this.inputs[i];
    return {
      open: target.open,
      close: target.close,
      high: target.high,
      low: target.low
    }
  }

  abandonedBaby(quote, i) {
    return abandonedbaby(this.buildInputs(i));
  }

  bearish(quote, i) {
    return bearish(this.buildInputs(i));
  }

  bearishEngulfingPattern(quote, i) {
    return bearishengulfingpattern(this.buildInputs(i));
  }

  bearishMarubozu(quote, i) {
    return bearishmarubozu(this.buildInputs(i));
  }

  bearishHarami(quote, i) {
    return bearishharami(this.buildInputs(i));
  }

  bearishHaramiCross(quote, i) {
    return bearishharamicross(this.buildInputs(i));
  }

  bearishSpinningTop(quote, i) {
    return bearishspinningtop(this.buildInputs(i));
  }

  darkCloudCover(quote, i) {
    return darkcloudcover(this.buildInputs(i));
  }

  downsideTasukiGap(quote, i) {
    return downsidetasukigap(this.buildInputs(i));
  }

  doji(quote, i) {
    return doji(this.buildInputs(i));
  }

  dragonFlyDoji(quote, i) {
    return dragonflydoji(this.buildInputs(i));
  }

  gravestoneDoji(quote, i) {
    return gravestonedoji(this.buildInputs(i));
  }

  bullish(quote, i) {
    return bullish(this.buildInputs(i));
  }

  bullishEngulfingPattern(quote, i) {
    return bullishengulfingpattern(this.buildInputs(i));
  }

  bullishHarami(quote, i) {
    return bullishharami(this.buildInputs(i));
  }

  bullishHaramiCross(quote, i) {
    return bullishharamicross(this.buildInputs(i));
  }

  bullishMarubozu(quote, i) {
    return bullishmarubozu(this.buildInputs(i));
  }

  bullishSpinningTop(quote, i) {
    return bullishspinningtop(this.buildInputs(i));
  }
  
  eveningDojiStar(quote, i) {
    return eveningdojistar(this.buildInputs(i));
  }

  eveningStar(quote, i) {
    return eveningstar(this.buildInputs(i));
  }

  morningDojiStar(quote, i) {
    return morningdojistar(this.buildInputs(i));
  }

  morningStar(quote, i) {
    return morningstar(this.buildInputs(i));
  }

  piercingLine(quote, i) {
    return piercingline(this.buildInputs(i));
  }

  threeBlackCrows(quote, i) {
    return threeblackcrows(this.buildInputs(i));
  }

  threeWhiteSoldiers(quote, i) {
    return threewhitesoldiers(this.buildInputs(i));
  }
}