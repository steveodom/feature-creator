import {targetArray} from './../utils/calcs';
import _ from 'lodash';
import round from 'lodash/round';


export default class CSCalcs {
  constructor(data) {
    this.data = data;
  }

  inputs(days = 3) {
    return this.data.map( (num, i) => {
      return this.calcWindowInputs(i + 1, days);
    });
  }

  calcWindowInputs(i, days) {
    let target = targetArray(i, days, this.data);
    let close = target.map ( (obj) => {return obj.close } );
    let open = target.map ( (obj) => {return obj.open } );
    let high = target.map ( (obj) => {return obj.high } );
    let low = target.map ( (obj) => {return obj.low } );

    if (high && low) {
      return {close, open, high, low};
    } else {
      return null;
    }
  }
}
