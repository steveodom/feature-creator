import {defaultRecipe} from './../sample-data';
import CMethods from './methods';
import CCalcs from './calcs';
import metaDetails from './meta';


export default class Candlesticks {
  constructor(series,recipe) {
    this.series = series;
    this.calcs = new CCalcs(series);
    this.recipe = recipe;
    if (!this.recipe.candlesticks) {
      this.recipe.candlesticks = [];
    }
  }

  init() {
    this.recipe.candlesticks.forEach( (item) => {
      item.methods = new CMethods(this.series, this.calcs.inputs(item.daysAgo));
    });
  }

  static token(recipeItem) {
    const baseName = 'cs';
    return `${baseName}-${recipeItem.feature}-${recipeItem.daysAgo}`;
  }

  static meta(recipeItem, klass) {
    const featureName = recipeItem.feature;
    try {
      return metaDetails[featureName](klass);
    } catch (err) {
      return null
    }    
  }

  build(quote, i) {
    let obj = {};

    this.recipe.candlesticks.forEach( (item) => {
      const methods = item.methods;
      
      const name = Candlesticks.token(item);
      obj[name] = methods[item.feature](quote, i, item);
    });
    return obj;
  }
}
