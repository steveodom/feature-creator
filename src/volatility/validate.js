import VMethods from './methods';
import _ from 'lodash';

const validateVolatility = (recipe = []) => {
  let isValid = true;
  let msg = [];
  const methods = new VMethods({}, {});

  let ary = recipe.map( (item) => {
    const a = item.hasOwnProperty('period');

    if (!a) {
      msg.push('Period field required');
    }

    const c = item.hasOwnProperty('feature');
    if (!c) {
      msg.push('Feature field required');
    }


    let e = typeof methods[item.feature] === 'function'
    if (!e) {
      msg.push(`VO Specified feature was not found: ${item.feature}`);
    }

    return a && c && e;
  })

  isValid = !_.includes(ary, false);
  return {isValid, msg};
}

export default validateVolatility;
