import {targetArray, arrayAvg, arrayDeviation} from './../utils/calcs';
import {min, max} from 'd3-array';

export default class VolatilityCalcs {
  constructor(data) {
    this.data = data;
  }

  rolling(days, klass = 'high') {
    return this.data.map( (quote, i) => {
      return this.calcAvgForWindow(i + 1, days, klass);
    });
  }

  calcAvgForWindow(i, days, klass) {
    let target = targetArray(i, days, this.data);
    let closes = target.map ( (obj) => obj[klass]);
    let avg = arrayAvg(closes);
    
    if (avg) {
      return avg
    } else {
      return 0;
    }
  }
}
