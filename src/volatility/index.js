import {defaultRecipe} from './../sample-data';
import VMethods from './methods';
import VCalcs from './calcs';
import metaDetails from './meta';


export default class Volatility {
  constructor(series,recipe) {
    this.series = series;
    this.calcs = new VCalcs(series);
    this.recipe = recipe;
    if (!this.recipe.volatility) {
      this.recipe.volatility = [];
    }
  }

  init() {
    this.recipe.volatility.forEach( (item) => {
      const highs = this.calcs.rolling(item.period, 'high');
      const lows = this.calcs.rolling(item.period, 'low');
      item.methods = new VMethods(this.series, highs, lows);
    });
  }

  static token(recipeItem) {
    const baseName = 'vo';
    return `${baseName}-${recipeItem.feature}-${recipeItem.period}`;
  }

  static meta(recipeItem, klass) {
    const featureName = recipeItem.feature;
    try {
      return metaDetails[featureName](klass);
    } catch (err) {
      return null
    }    
  }

  build(quote, i) {
    let obj = {};

    this.recipe.volatility.forEach( (item) => {
      const methods = item.methods;
      
      const name = Volatility.token(item);
      obj[name] = methods[item.feature](quote, i, item);
    });
    return obj;
  }
}
