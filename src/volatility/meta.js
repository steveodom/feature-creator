const chartDetails = {
  ratio(klass) {
    return {
      group: 'Price Data',
      hidden: true,
      yAxisID: 'right',
      label: 'Volatility Ratio',
      type: 'line',
      key: `${klass}-volRatio`,
      pointRadius: 6,
      showLine: false,
      normalize: 'quantile'
    }
  }
};

export default chartDetails;
