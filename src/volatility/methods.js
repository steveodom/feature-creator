import { nextDayChange, withinQuoteScope } from './../utils/calcs';
import _ from 'lodash';

export default class VolMethods {
  constructor(series, highs, lows) {
    this.series = series;
    this.highs = highs;
    this.lows = lows;
  }

  // Current True Range = Maximum (average of current day's high and yesterday's close) - Minimum (average of today's low and yesterday's close)
  trueRange(quote, i) {
    const today = quote;
    const yesterday = this.series[i - 1];
    if (!yesterday || !yesterday.hasOwnProperty('close')) return 0;
    const high = _.mean([today.high, yesterday.close]);
    const low = _.mean([today.low, yesterday.close]); 
    return _.round(high - low, 2);
  }

  //Previous True Range over X number of days = HIGH (average of the high prices of each day over time period X) - LOW (average of the low prices of each day over time period X)
  prevTrueRange(i) {
    const high = this.highs[i];
    const low = this.lows[i];
    return _.round(high - low, 2);
  }

  ratio(quote, i) {
    return _.round(this.trueRange(quote, i) / this.prevTrueRange(i), 2); 
  }
}
