import {arrayChunks} from './../utils/calcs';
import {min, max, mean, range } from 'd3-array';
import {scaleQuantize } from 'd3-scale';

export default class SparkLabels {
  constructor(data, granularity = 4, labelType = '0001', useAverageRange = false) { 
    this.data = data;
    this.granularity = granularity;
    this.windowLength = granularity;
    let mn = min(data);
    let mx = max(data);
    this.labelType = labelType;

    this.chunks = arrayChunks(data, this.windowLength, true);
    const averages = this.chunks.map( (ch) => (mean(ch)));

    if (useAverageRange) {
      mn = min(averages);
      mx = max(averages)
    }
    
    let words = this.labels();

    this.word = scaleQuantize()
      .domain([mn, mx])
      .range(words);
  }

  labels() {
    let pos = '1';
    let neg = '0';

    if (this.labelType === 'str') {
      pos = 'l';
      neg = 'o';
    }

    if (this.labelType === 'strAlt') {
      pos = 'u';
      neg = 'd';
    }


    // Creates an array moving from the bottom to the top, which is the range to choose from.
    // An equivalent is if you wanted to generate a range of colors between red and blue. 
    // whatever the avg is for that period, the scaleQuantize will assign it from this range.
    return range(0, this.granularity).map( (item, i) => {
      // populate the whole string with the negative version
      let str = neg.repeat(this.granularity);
      // replace the target cell with the pos. ;
      return str.substr(0, i) + pos + str.substr(i+1);
    });
  }

  columns() {
    let word = this.word;
    return this.chunks.map( (chunk) => {
      let avg = mean(chunk);
      return word(avg);
    });
  }
}
