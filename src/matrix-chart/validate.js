import MatrixChartMethods from './methods';
import _ from 'lodash';


const validateMatrixChart = (recipe = []) => {
  let isValid = true;
  let msg = [];
  const methods = new MatrixChartMethods({}, {});

  let ary = recipe.map( (item) => {
    const a = item.hasOwnProperty('granularity');

    if (!a) {
      msg.push('Granularity field required');
    }

    const b = item.hasOwnProperty('type');
    if (!b) {
      msg.push('Type field required');
    }

    const c = item.hasOwnProperty('feature');
    if (!c) {
      msg.push('Feature field required');
    }

    // const d = item.hasOwnProperty('name');
    // if (!d) {
    //   msg.push('Name field required');
    // }

    let e = typeof methods[item.feature] === 'function'
    if (!e) {
      msg.push(`Specified MC feature was not found: ${item.feature}`);
    }

    return a && b && c && e;
  })

  isValid = !_.includes(ary, false);
  return {isValid, msg};
}

export default validateMatrixChart;
