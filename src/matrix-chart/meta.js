const meta = {
  chart(klass) {
    return {
      group: 'MatrixChart',
      hidden: true,
      yAxisID: 'left',
      label: 'Matrix Chart',
      type: 'line',
      key: `${klass}-maxtrixChart`,
      pointRadius: 0,
      normalize: 'ngram'
    }
  },
  stringChart(klass) {
    return {
      group: 'MatrixChart',
      hidden: true,
      yAxisID: 'left',
      label: 'Matrix Chart',
      type: 'line',
      key: `${klass}-maxtrixChart`,
      pointRadius: 0,
      normalize: 'ngram'
    }
  },
  isFlatPattern(klass) {
    return {
      group: 'MatrixChart',
      hidden: false,
      yAxisID: 'right',
      label: 'isFlatPattern',
      type: 'line',
      key: `${klass}-maxtrixChart`,
      showLine: false
    }
  },
  isUpOrDownPattern(klass) {
    return {
      group: 'MatrixChart',
      hidden: false,
      yAxisID: 'right',
      label: 'isUpOrDownPattern',
      type: 'line',
      key: `${klass}-maxtrixChart`,
      showLine: false
    }
  },
  isBelowToAbovePattern(klass) {
    return {
      group: 'MatrixChart',
      hidden: false,
      yAxisID: 'right',
      label: 'isBelowToAbovePattern',
      type: 'line',
      key: `${klass}-maxtrixChart`,
      showLine: false
    }
  },
  isHighVolumeAndBelowToAbove(klass) {
    return {
      group: 'MatrixChart',
      hidden: false,
      yAxisID: 'right',
      label: 'isHighVolumeAndBelowToAbove',
      type: 'line',
      key: `${klass}-maxtrixChart`,
      showLine: false
    }
  }
}

export default meta;
