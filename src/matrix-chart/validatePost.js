import MatrixChartMethods from './methods';
import _ from 'lodash';

// matrixChartPost: [
//    {feature: 'isFlatPattern', 'period': 5, type: 'closes'}
//],
const validateMatrixChartPost = (recipe = []) => {
  let isValid = true;
  let msg = [];
  const methods = new MatrixChartMethods({}, {});

  let ary = recipe.map( (item) => {
    

    const b = item.hasOwnProperty('type');
    if (!b) {
      msg.push('Type field required');
    }

    const c = item.hasOwnProperty('feature');
    if (!c) {
      msg.push('Feature field required');
    }

    let e = typeof methods[item.feature] === 'function'
    if (!e) {
      msg.push('Specified feature was not found');
    }

    return b && c && e;
  })

  isValid = !_.includes(ary, false);
  return {isValid, msg};
}

export default validateMatrixChartPost;
