import _ from 'lodash';
import MatrixChartMethods from './methods';
import MXCalcs from './calcs';
import metaDetails from './meta';
import SRCalcs from './../support-resistance/calcs';

export default class MatrixChart {
  constructor(series,recipe, indexSeries) {
    this.series = series;
    this.recipe = recipe;
    this.closes = series.map ( (q) => {return q.close } );
    this.volume = series.map ( (q) => {return q.volume } );
    
    this.obv = SRCalcs.obv(this.closes, this.volume);
    
    if (indexSeries) {
      this.indexCloses = _.values(indexSeries).map ( (q) => {return q.close } );
    } 
  }

  init() {
    this.recipe.matrixChart.forEach( (item) => {
      let calcs;

      switch (item.type) {
      case 'closes':
        calcs = new MXCalcs(this.closes, item.granularity, 'str', item.useAverageRange);
        break;
      case 'volume':
        calcs = new MXCalcs(this.volume, item.granularity, 'str', item.useAverageRange);
        break;
      case 'obv':
        calcs = new MXCalcs(this.obv, item.granularity, 'str', item.useAverageRange);
        break;
      case 'indexCloses':
        calcs = new MXCalcs(this.indexCloses, item.granularity, 'strAlt', item.useAverageRange);
        break;
      default:
        calcs = new MXCalcs(this.closes, item.granularity, 'str', item.useAverageRange);
      }

      item.methods = new MatrixChartMethods(this.series, calcs);
    });
  }

  static token(recipeItem) {
    const baseName = 'mx';
    const typeInitial = recipeItem.type[0];
    return `${baseName}${typeInitial}-${recipeItem.period}`;
  }

  static postToken(recipeItem) {
    const baseName = 'mx';
    const typeInitial = recipeItem.type[0];
    return `${baseName}${typeInitial}-${recipeItem.period}-${recipeItem.feature}`;
  }

  static meta(recipeItem, klass) {
    const featureName = recipeItem.feature;
    try {
      return metaDetails[featureName](klass);
    } catch (err) {
      return null
    }    
  }

  build(quote, i) {
    let obj = {};
    this.recipe.matrixChart.forEach( (item) => {
      const methods = item.methods;
      const name = MatrixChart.token(item);
      obj[name] = methods[item.feature](item.period, i, obj);
    });
    return obj;
  }

  post(feature) {
    let obj = {};
    if (!this.recipe.matrixChartPost || !Array.isArray(this.recipe.matrixChartPost)) {
      return obj;
    }
    this.recipe.matrixChartPost.forEach( (item, i) => {
      const base = this.recipe.matrixChart[i];
      const methods = base.methods;
      const name = MatrixChart.postToken(item);
      const chartToken = MatrixChart.token(item);
      let chartTokenSecond;
      let chartSecond = '';
      if (item.type === 'isHighVolumeAndBelowToAbove') {
        chartTokenSecond = MatrixChart.token({type: 'volume', period: item.period});
      }
      const chart = feature[chartToken];
      chartSecond = feature[chartTokenSecond];
      obj[name] = methods[item.feature](chart, chartSecond);
    });
    return obj;
  }
}
