
export default class MatrixChartMethods {
  constructor(series, calcs) {
    this.series = series;
    this.calcs = calcs;
  }

  chart(period = 1, i) {
    return this.calcs.chart(period, i);
  }

  stringChart(period=1, i) {
    const charts = this.chart(period, i);
    return charts.join('-');
  }

  isInvalidPattern(chart) {
    return !chart || typeof(chart) !== 'string' || !chart.includes('-');
  }

  isFlatPattern(chart) {
    if (this.isInvalidPattern(chart)) return false;
    const [p1, p2, p3, p4] = chart.split('-');
    return (p1 === 'looo' && p4 === 'oool') ||
      (p1 === 'oool' && p4 === 'looo') ||
      (p1 === 'looo' && p3 === 'oool') ||
      (p1 === 'oool' && p3 === 'looo')
  }

  isUpOrDownPattern(chart) {
    if (this.isInvalidPattern(chart)) return false;
    const [p1, p2, p3, p4] = chart.split('-');
    return (p2 === 'oloo' && p4 === 'oolo');
  }

  isBelowToAboveBasic(chart) {
    if (this.isInvalidPattern(chart)) return false;
    const [p1, p2, p3, p4] = chart.split('-');
    return (p1 === 'oolo' && p2 === 'oolo' && p4 === 'oloo');
  }

  isBelowToAboveExtended(chart) {
    if (this.isInvalidPattern(chart)) return false;
    const [p1, p2, p3, p4] = chart.split('-');
    return (p1 === 'oool' && p2==='oolo' && p4 === 'oloo') ||
      (p2 === 'oool' && p1==='oolo' && p4 === 'oloo');
  }

  isBelowToAbovePattern(chart) {
    return this.isBelowToAboveBasic(chart) || this.isBelowToAboveExtended(chart);
  }

  isHighVolume(volumeChart) {
    if (this.isInvalidPattern(volumeChart)) return false;
    const [p1, p2, p3, p4] = volumeChart.split('-');
    return p4 === 'oolo' || p4 === 'oool';
  }

  isHighVolumeAndBelowToAbove(closeChart, volumeChart) {
    return this.isHighVolume(volumeChart) && this.isBelowToAboveBasic(closeChart);
  }
}
