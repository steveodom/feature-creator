import {determineStart, arrayWindow} from './../utils/calcs';
import SparkLabels from './spark-labels';

export default class MatrixChart {
  constructor(data, granularity = 4, labelType = '0001', useAverageRange = false) {
    this.data = data;
    this.granularity = granularity; 
    this.labelType = labelType;
    // this.chunks = chunks || granularity; // would it ever not be square 4x4, 8x8. If not, consider making chunks granularity
    this.useAverageRange = useAverageRange;
  }

  chart(days, i) {
    return this.arrayOfSparkLabels(i + 1, days);
  }

  arrayOfSparkLabels(i, days) {
    let {start, l} = determineStart(i, days * this.granularity);
    let target = arrayWindow(this.data, start, l);
    return new SparkLabels(target, this.granularity, this.labelType, this.useAverageRange).columns();
  }
}
