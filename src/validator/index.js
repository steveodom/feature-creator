import validateMatrixChart from './../matrix-chart/validate';
import validateMatrixChartPost from './../matrix-chart/validatePost';
import validateMovingAverage from './../moving-averages/validate';
import validateBasic from './../basic/validate';
import validateChange from './../change/validate';
import validateSupportResistance from './../support-resistance/validate';
import validateRelativeStrength from './../relative-strength/validate';
import validateVol from './../volatility/validate';

const validator = (recipe, indexSeries) => {
  const validChange = validateChange(recipe.changeCalc);
  if (!validChange.isValid) {
    return validChange;
  }

  const validMatrixChart = validateMatrixChart(recipe.matrixChart);
  if (!validMatrixChart.isValid) {
    return validMatrixChart;
  }

  const validMatrixChartPost = validateMatrixChartPost(recipe.matrixChartPost);
  if (!validMatrixChartPost.isValid) {
    return validMatrixChartPost;
  }

  const validMovingAverage = validateMovingAverage(recipe.movingAverage);
  if (!validMovingAverage.isValid) {
    return validMovingAverage;
  }

  const validRelativeStrength = validateRelativeStrength(recipe.relativeStrength, indexSeries);
  if (!validRelativeStrength.isValid) {
    return validRelativeStrength;
  }

  const validSupportResistance = validateSupportResistance(recipe.supportResistance);
  if (!validSupportResistance.isValid) {
    return validSupportResistance;
  }

  const validVol = validateVol(recipe.volatility);
  if (!validVol.isValid) {
    return validVol;
  }

  const validBasic = validateBasic(recipe.basic);
  if (!validBasic.isValid) {
    return validBasic;
  }

  return {isValid: true, msg: []};
}

export default validator;
