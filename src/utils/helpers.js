import moment from 'moment';
import _ from 'lodash';
import {dates} from 'namer';

export const identifiers = (quote, ticker) => {
  return {
    ticker,
    Date: displayDate(quote),
    roundedDate: displayDate(quote)
  }
}

export const padTechnicalIndicators = (output, indicatorSize) => {
  if (output.length) {
    const diff = indicatorSize - output.length;
    const pad = _.fill(Array(diff), 0)
    return [...pad, ...output];
  } else {
    return [];
  }
}

export const displayDate = (quote) => {
  if (quote.Timestamp) {
    return dates.display(quote.Timestamp, 'realtime', 'yahooQuotes', 'output', true);
  } else {
    return dates.display(quote.Date, 'daily', 'yahooQuotes', 'output');
  }
}

export const removeProperties = (quote, removeArray) =>  {
  const modified = Object.assign({}, quote);
  removeArray.forEach((key) => (delete modified[key]));
  return modified;
};
