import drop from 'lodash/drop';
import round from 'lodash/round';
import {sum, mean, quantile, min, max, ticks, deviation, variance} from 'd3-array';
import { scaleQuantize } from 'd3-scale';

export const targetArray = (i, days, series) => {
  let {start, l} = determineStart(i, days);
  return arrayWindow(series, start, l);
}

export const targetHighsAndLows = (target) => {
  const highs = target.map ( (obj) => {return obj.high } );
  const lows = target.map ( (obj) => {return obj.low } );

  const high = max(highs);
  const low = min(lows);
  return {high, low};
}

export const nextDayChange = (today, tomorrow) => {
  if (tomorrow && tomorrow.close && today && today.close) {
    let change = tomorrow.close - today.close;
    let pct = change / today.close * 100;

    return round(pct, 2);
  }
  return 0;
}

export const nextDayChangeGeneric = (today, tomorrow) => {
  if (tomorrow && today) {
    let change = tomorrow - today;
    let pct = change / today * 100;

    return round(pct, 2);
  }
  return 0;
}

export const determineStart = (i, days) => {
  let start = i - days;
  let l = days;

  if (start < 0 ) {
    start = 0;
    l = i;
  }
  return {start, l};
}

export const arrayStartPosition = (items, offset) => {
  return drop(items, offset);
}

export const arrayWindow = (ary = [], offset = 0, days) => {
  let start = arrayStartPosition(ary, offset);
  return start.slice(0, days);
}

export const arrayAdd = (ary) => {
  return sum(ary);
}

export const arrayAvg = (ary) => {
  return round(mean(ary),2);
}

export const arrayDeviation = (ary) => {
  return deviation(ary);
}

export const arrayVariance = (ary) => {
  return variance(ary);
}

export const arrayQuartiles = (ary) => {
  let low = min(ary);
  let high = max(ary);
  let q1 = quantile([low, high], 0.25);
  let q2 = quantile([low, high], 0.50);
  let q3 = quantile([low, high], 0.75);
  return {q1, q2, q3}
}

export const arrayTicks = (ary) => {
  let low = min(ary);
  let high = max(ary);
  return ticks(low, high, 3);
}

export const closeRange = (close, low, high) => {
  let range = high - low;
  let diff = close - low;
  return round(diff / range, 2);
}

// source: http://stackoverflow.com/questions/8188548/splitting-a-js-array-into-n-arrays
// lodash has chunks, but I like how this handles evening out the results.
export const arrayChunks = (a, n, balanced) => {
  if (n < 2) return [a];

  let len = a.length;
  let out = [];
  let i = 0;
  let size;

  if (len % n === 0) {
    size = Math.floor(len / n);
    while (i < len) {
      out.push(a.slice(i, i += size));
    }
  } else if (balanced) {
    while (i < len) {
      size = Math.ceil((len - i) / n--);
      out.push(a.slice(i, i += size));
    }
  } else {
    n--;
    size = Math.floor(len / n);
    if (len % size === 0)
      size--;
    while (i < size * n) {
      out.push(a.slice(i, i += size));
    }
    out.push(a.slice(size * n));
  }
  return out;
}

export const isWithin = (subject, target, threshold = 0.01) => {
  const mx = target * (1 + threshold);
  const mn = target * (1 - threshold);
  return subject >= mn && subject <= mx;
}

export const binnedNum = (num) => {
  const scale = scaleQuantize()
    .domain([0, 15130000])
    .range([0,1,2,3,4]);
  return scale(num);
}

export const withinQuoteScope = (i, daysAgo) => {
  return i - daysAgo > 0;
}
