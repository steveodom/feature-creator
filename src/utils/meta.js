import _ from 'lodash';

import basic from './../basic';
import ma from './../moving-averages';

export default class QuoteGroup {
  constructor(quote) {
    this.labels = Object.keys(quote);
    this.chartable = [];
  }

  find(group = 'basic') {
    return this.labels.filter( (label) => (label.startsWith(group)));
  }

  findAndRemove(group = 'basic', remove='basic-') {
    const targets = this.find(group);
    return targets.map( (label) => (label.replace(remove, '')));
  }

  checkMeta(group, feature, label = '', klass) {
    let res = group.meta(feature, klass);
    if (res) {
      if (!_.isEmpty(label)) {
        res.label = res.label += `-${label}`;
        res.key = res.key += `-${label}`;
      }
      this.chartable.push(res);
    }
  }

  buildBasic() {
    const keys = this.findAndRemove('basic', 'basic-');
    const all = [...keys, 'close', 'high', 'low'];
    all.forEach( (feature) => (this.checkMeta(basic, feature)));
  }

  buildMovingAverage() {
    let base = 'mar';
    let keys = this.findAndRemove(base, `${base}-`);
    keys.forEach( (feature) => {
      const modified = feature.split('-');
      this.checkMeta(ma, {feature: modified[0]}, modified[1], base);
    });

    base = 'mad';
    keys = this.findAndRemove(base, `${base}-`);
    keys.forEach( (feature) => {
      const modified = feature.split('-');
      this.checkMeta(ma, {feature: modified[0]}, modified[1], base);
    });
  }

  build() {
    this.buildBasic();
    this.buildMovingAverage();
    return this.chartable;
  }

  buildAndGroup() {
    const features = this.build();
    let structured = {};
    const groups = _.groupBy(features, (obj) => (obj.group));
    Object.keys(groups).forEach( (key) => {
      const group = groups[key];
      structured[key] = {datasets: group}
    })
    return structured;
  }
}


// export const fetchDetails = (base, feature) =>  {
//   // console.info(base, [base], 'fetchDetails');
//   if ([base][feature]) {
//     return [base][feature]();
//   } else {
//     return null;
//   }
// };


// export const featureChartObject = (name) =>  {
//   const components = name.split('-');
//   let base = components[0];
//   let feature = components[1];
//   if (['close', 'high', 'low', 'open', 'volume'].includes(base)) {
//     feature = base;
//     base = 'basic';
//   }
//   return fetchDetails(base, feature);
// };

// export const chartableFeatures = (quote) =>  {
//   return _.compact(Object.keys(quote).map( (key) => (featureChartObject(key))));
// };

// export const chartableFeaturesByGroup = (quote) =>  {
//   let structured = {};
//   const features = chartableFeatures(quote);
//   const groups = _.groupBy(features, (obj) => (obj.group));
//   Object.keys(groups).forEach( (key) => {
//     const group = groups[key];
//     structured[key] = {datasets: group}
//   })
//   return structured;
// };




