import Promise from 'bluebird';
import _ from 'lodash';
import getQuotes from 'quote-fetching';
import {fetchDateBased} from 'trading-quotes-saver'
import hydrateWithFeatures from './features';
import {fetch} from './utils/s3';
import QuoteGroup from './utils/meta';
import RecipeGroup from './recipeGroups';
import MACalcs from './moving-averages/calcs';

// expects oldest first
export const fetchAndHydrate = (ticker, period, purpose, namer, indexSeries) => {
  let quotes;
  if (period === '14d') {
    quotes = fetchDateBased(ticker, period);
  } else {
    quotes = getQuotes(ticker, period);
  }
  
  let series;
  
  return quotes.then( (res) => {
    series = res;
    return fetch('trading-features', `${namer.directories.recipes}/index.json`);
  }).then( (res) => {
    let recipe = JSON.parse(res);
    return hydrateWithFeatures(series.series, recipe, ticker, indexSeries);
  }).then( (res) => {
    if (res.hasOwnProperty('isValid') && !res.isValid) {
      return Promise.reject(res.msg.join(', '));
    }
    if (purpose === 'forecast') {
      // shift takes the first element
      // res.slice(0,1) takes first element
      return Promise.resolve(_.last(res));
    } else {
      return Promise.resolve(res);
    }
  })
  .catch( (err) => {
    return Promise.reject(err);
  });
}

export const currentN = (quotes, period) => {
  const ma = new MACalcs(quotes);
  const trs = ma.trueRange();
  const averages = ma.avgTrueRange(trs, period);
  const n = _.last(averages);
  const nPct = _.round(n / _.last(quotes).close, 5);
  const rolling = ma.rolling(period);
  const lastBB = _.last(rolling.bb);
  const twoStdDevPctChange = _.round((lastBB.upper - lastBB.middle) / lastBB.middle, 4);
  return {
    tr: _.last(trs), 
    n, 
    nPct,
    twoStdDevPctChange,
    std: ma.seriesDeviationOfCloses(),
    stdV: ma.seriesDeviationOfVolume()
  };
};

export const chartableFeatures = (quote) => {
  return new QuoteGroup(quote).buildAndGroup();
};

export const buildAwsRecipeGroups = (recipe) => {
  return new RecipeGroup().build(recipe);
}

export default fetchAndHydrate;