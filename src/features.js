import {defaultSeries} from './sample-data';
import {identifiers, removeProperties} from './utils/helpers.js';

import validator from './validator';

import Basic from './basic';
import Change from './change';

import MovingAverages from './moving-averages';
import MatrixChart from './matrix-chart';
import SupportResistance from './support-resistance';
import RelativeStrength from './relative-strength';
import Volatility from './volatility';
import Candlesticks from './candlesticks';

const hydrateWithFeatures = (series = defaultSeries, recipe, ticker, indexSeries) => {

  const isValidRecipe = validator(recipe, indexSeries);

  if (isValidRecipe && !isValidRecipe.isValid) {
    return isValidRecipe;
  }

  const r = recipe;

  const b = new Basic(series, r);
  b.init();

  const ma = new MovingAverages(series, r, indexSeries);
  ma.init();

  // change is below MovingAverage because it uses its std dev calc
  const c = new Change(series, r);
  c.init(ma.recipe.movingAverage);

  const mc = new MatrixChart(series, r, indexSeries);
  mc.init();

  const sr = new SupportResistance(series, r);
  sr.init();

  const rs = new RelativeStrength(series, r, indexSeries);
  rs.init();

  const vo = new Volatility(series, r);
  vo.init();

  const cs = new Candlesticks(series, r);
  cs.init();

  const obj = series.map( (quote, i) => {
    let modified = quote;
    
    if (r.removeFromQuote) {
      modified = removeProperties(quote, r.removeFromQuote);
    }
    
    return Object.assign(
      modified,
      identifiers(quote, ticker),
      c.build(quote, i),
      b.build(quote, i),
      ma.build(quote, i),
      mc.build(quote, i),
      sr.build(quote, i),
      rs.build(quote, i),
      vo.build(quote, i),
      cs.build(quote, i)
    );
  });

  return obj.map( (feature, i) => {
    return Object.assign(
      feature,
      mc.post(feature, i)
    )
  })
}

export default hydrateWithFeatures;
