import RSMethods from './methods';
import _ from 'lodash';


const validateRelativeStrength = (recipe = [], indexSeries = []) => {
  let isValid = true;
  let msg = [];
  const methods = new RSMethods({}, {});

  let ary = recipe.map( (item) => {
    const a = item.hasOwnProperty('daysAgo');

    if (!a) {
      msg.push('daysAgo field required');
    }

    const b = item.hasOwnProperty('feature');
    if (!b) {
      msg.push('Feature field required');
    }

    let c = typeof methods[item.feature] === 'function'
    if (!c) {
      msg.push(`RS Specified feature was not found: ${item.feature}`);
    }

    return a && b && c;
  });

  // can pass in false as indexSeries arg to skip
  if ( !_.isEmpty(recipe) && !_.isBoolean(indexSeries) && (_.isEmpty(indexSeries) || typeof(indexSeries) !== 'object')) {
    return {isValid: false, msg: ['a valid indexSeries which is an object with timeindex as keys must be passed in']}
  }

  isValid = !_.includes(ary, false);
  return {isValid, msg};
}

export default validateRelativeStrength;
