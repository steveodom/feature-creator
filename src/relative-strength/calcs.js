import _ from 'lodash';
import {targetArray, arrayAvg} from './../utils/calcs';
import {padTechnicalIndicators} from './../utils/helpers';
import {dates} from 'namer';
import {helpers} from 'quote-fetching';
const {dateBasedToken} = helpers.intraday;

import round from 'lodash/round';
import {RSI} from 'technicalindicators';

export const getIndexByDates = (startQuote, endQuote, indexSeries) => {
  let startToken;
  let endToken;

  if (startQuote.hasOwnProperty('Date')) {
    startToken = dates.display(startQuote.Date, 'daily', 'yahooQuotes', 'output', false);
    endToken = dates.display(endQuote.Date, 'daily', 'yahooQuotes', 'output', false);
  }

  if (startQuote.hasOwnProperty('Timestamp')) {
    startToken = dateBasedToken(startQuote.Timestamp, 5);
    endToken = dateBasedToken(endQuote.Timestamp, 5);
  }
  return {startQuoteIndex: indexSeries[startToken], endQuoteIndex: indexSeries[endToken]};
}

export default class RSCalcs {
  constructor(data) {
    this.data = data;
    this.closes = data.map ( (obj) => {return obj.close } );
  }

  rsiList(days = 14) {
    const input = { values: this.closes, period: days};
    const avgs = RSI.calculate(input);
    const res = padTechnicalIndicators(avgs, this.closes.length);
    return res;
  }

  calcAvgForWindow(i, days) {
    let target = targetArray(i, days, this.data);
    let closes = target.map ( (obj) => obj.close);
    let avg = arrayAvg(closes);
 
    if (avg) {
      return {avg};
    } else {
      return {avg: 0};
    }
  }
}