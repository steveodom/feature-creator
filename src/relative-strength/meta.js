const meta = {
  relativePct(klass) {
    return {
      group: 'Price Data',
      hidden: true,
      yAxisID: 'right',
      label: 'Relative Pct',
      type: 'line',
      key: `${klass}-relativePct`,
      pointRadius: 6,
      normalize: 'quantile'
    }
  },
  indexChange(klass) {
    return {
      group: 'Price Data',
      hidden: true,
      yAxisID: 'right',
      label: 'Relative Pct',
      type: 'line',
      key: `${klass}-relativePct`,
      pointRadius: 6
    }
  },
  rsi(klass) {
    return {
      group: 'Price Data',
      hidden: true,
      yAxisID: 'right',
      label: 'RSI',
      type: 'line',
      key: `${klass}-rsi`
    }
  }
}

export default meta;
