import Promise from 'bluebird';
import _ from 'lodash';
// import fetch, {fetchByDate} from 'quote-fetching';
import { nextDayChange, withinQuoteScope } from './../utils/calcs';
import {getIndexByDates} from './calcs';

export default class RSMethods {
  constructor(series, averages, indexSeries) {
    this.series = series;
    this.averages = averages;
    this.indexSeries = indexSeries;
  }

  rsi(quote, i) {
    return this.averages[i] || 0;
  }

  getEndQuote(i, daysAgo) {
    return this.series[i - daysAgo];
  }

  getStockChange(quote, endQuote) {
    return nextDayChange(endQuote, quote);
  }

  calcChanges(quote, i, lookback) {
    const endQuote = this.getEndQuote(i, lookback);
    const stockChange = this.getStockChange(quote, endQuote);

    const res = getIndexByDates(quote, endQuote, this.indexSeries)
    const {endQuoteIndex, startQuoteIndex} = res;
    const indexChange = this.getStockChange(startQuoteIndex, endQuoteIndex);

    return {stockChange, indexChange}
  }

  relativePct(quote, i, options = {}) {
    const lookback = options.daysAgo || options.period || 14;
    if (!withinQuoteScope(i, lookback)) return 0;
    const {stockChange, indexChange} = this.calcChanges(quote, i, lookback);
    return _.round(stockChange / indexChange, 2);
  }

  indexChange(quote, i, options = {}) {
    const lookback = options.daysAgo || options.period || 14;
    if (!withinQuoteScope(i, lookback)) return 'flat';
    const {indexChange} = this.calcChanges(quote, i, lookback);
    const high = 0.15;
    const mid = 0;
    const low = -0.15;

    if (indexChange > high) {
      return 'up';
    } else if (indexChange <= low) {
      return 'down';
    } else if (indexChange <= high && indexChange > mid) {
      return 'up-flat';
    } else if (indexChange <= mid && indexChange > low) {
      return 'down-flat';
    } else {
      return 'flat';
    }
  }
}
