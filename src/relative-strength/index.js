import Promise from 'bluebird';
import RSCalcs from './calcs';
import Methods from './methods';
import metaDetails from './meta';
import fetch from 'quote-fetching';
import _ from 'lodash';

export default class RelativeStrength {
  constructor(series,recipe,indexSeries) {
    this.series = series;
    this.calcs = new RSCalcs(series);
    this.recipe = recipe;
    this.indexSeries = indexSeries;
  }

  init() {
    let averages;
    if (_.isEmpty(this.recipe.relativeStrength)) return null;
    this.recipe.relativeStrength.forEach( (item) => {
      switch (item.type) {
      case 'rsi':
        averages = this.calcs.rsiList(item.daysAgo);
        break;
      default:
        averages = {};
      }
      item.methods = new Methods(this.series, averages, this.indexSeries);
    });
  }

  static token(recipeItem) {
    const baseName = 'rsi';
    return `${baseName}-${recipeItem.feature}-${recipeItem.daysAgo}`;
  }

  static meta(recipeItem) {
    const featureName = recipeItem.feature;
    const klass = recipeItem.daysAgo;
    try {
      return metaDetails[featureName](klass);
    } catch (err) {
      return null
    }    
  }

  isInvalidIndexToCompare() {
    return typeof(this.indexSeries) !== 'object' || _.isEmpty(this.indexSeries)
  }

  build(quote, i) {
    let obj = {};
    if (_.isEmpty(this.recipe.relativeStrength) || this.isInvalidIndexToCompare() ) return obj;
    this.recipe.relativeStrength.forEach( (item) => {
      const methods = item.methods;
      const name = RelativeStrength.token(item);
      obj[name] = methods[item.feature](quote, i, item);
    });
    return obj;
  }
}
