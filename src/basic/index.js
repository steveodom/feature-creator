import {defaultRecipe} from './../sample-data';
import BasicMethods from './methods';
import metaDetails from './meta';

import _ from 'lodash';

export default class Basic {
  constructor(series, recipe) {
    this.methods = new BasicMethods(series);
    this.recipe = recipe;
  }

  init() {}

  static token(feature) {
    const baseName = 'basic';
    if (_.includes(['close', 'high', 'low', 'open', 'volume'], feature)) {
      return feature;
    } else {
      return `${baseName}-${feature}`;
    }
  }

  static meta(featureName) {
    try {
      return metaDetails[featureName]();
    } catch (err) {
      return null
    }    
  }

  build(quote, i) {
    let obj = {};
    this.recipe.basic.forEach( (name) => {
      const key = Basic.token(name);
      obj[key] = this.methods[name](quote);
    });

    return obj;
  }
}
