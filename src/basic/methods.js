import round from 'lodash/round';
import {closeRange} from './../utils/calcs';
import {defaultQuote} from './../sample-data';
import {intradayIndex, ticksToEndOfDay} from './calcs'

export default class BasicMethods {
  constructor(series) {
    this.series = series;
  }

  _logQuote(quote, key) {
    return round(Math.log(quote[key]), 3);
  }

  logClose(quote) {
    return this._logQuote(quote, 'close');
  }

  logHigh(quote) {
    return this._logQuote(quote, 'high');
  }

  logLow(quote) {
    return this._logQuote(quote, 'low');
  }

  logOpen(quote) {
    return this._logQuote(quote, 'open');
  }

  closeAtHigh(quote = defaultQuote) {
    return quote.close === quote.high;
  }

  closeAtLow(quote = defaultQuote) {
    return quote.close === quote.low;
  }

  closeRangePct(quote = defaultQuote) {
    return closeRange(quote.close, quote.low, quote.high);
  }

  intradayIndex(quote, i, options = {}) {
    if (quote && quote.Timestamp) {
      return intradayIndex(quote.Timestamp, options.increment);
    } else {
      return 'na'
    }
  }

  ticksToEndOfDay(quote, i, options = {}) {
    if (quote && quote.Timestamp) {
      return ticksToEndOfDay(quote.Timestamp, options.increment);
    } else {
      return 'na'
    }
  }
}
