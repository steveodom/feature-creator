const meta = {
  close() {
    return {
      group: 'Price Data',
      hidden: false,
      yAxisID: 'left',
      label: 'Close',
      type: 'line',
      key: 'close',
      pointRadius: 0,
      normalize: 'normalize'
    }
  },
  high() {
    return {
      group: 'Price Data',
      hidden: true,
      yAxisID: 'left',
      label: 'High',
      type: 'line',
      key: 'high',
      pointRadius: 0,
      normalize: 'normalize'
    }
  },
  low() {
    return {
      group: 'Price Data',
      hidden: true,
      yAxisID: 'left',
      label: 'Low',
      type: 'line',
      key: 'low',
      pointRadius: 0,
      normalize: 'normalize'
    }
  },
  volume() {
    return {
      group: 'Price Data',
      hidden: true,
      yAxisID: 'left',
      label: 'Volume',
      type: 'line',
      key: 'volume',
      pointRadius: 0,
      normalize: 'normalize'
    }
  },
  logClose() {
    return {
      group: 'Log Data',
      hidden: false,
      yAxisID: 'left',
      label: 'Log Close',
      type: 'line',
      key: 'basic-logClose',
      pointRadius: 0
    }
  },

  closeAtHigh() {
    return {
      group: 'Price Data',
      hidden: false,
      yAxisID: 'right',
      label: 'Close at High',
      type: 'line',
      key: 'basic-closeAtHigh',
      showLine: false
    }
  },
  closeAtLow() {
    return {
      group: 'Price Data',
      hidden: false,
      yAxisID: 'right',
      label: 'Close at Low',
      type: 'line',
      key: 'basic-closeAtLow',
      showLine: false
    }
  },
  closeRangePct() {
    return {
      group: 'Price Data',
      hidden: false,
      yAxisID: 'left',
      label: 'Close Range Pct',
      type: 'line',
      key: 'closeRangePct',
      pointRadius: 0,
      normalize: 'quantile'
    }
  },
};

export default meta;
