import BasicMethods from './methods';
import _ from 'lodash';


const validateBasic = (recipe = []) => {
  let isValid = true;
  let msg = [];
  const methods = new BasicMethods({}, {});

  let ary = recipe.map( (name) => {
    let e = typeof methods[name] === 'function'
    if (!e) {
      msg.push('Specified feature was not found');
    };

    return e;
  })

  isValid = !_.includes(ary, false);
  return {isValid, msg};
}

export default validateBasic;
