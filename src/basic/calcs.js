import {helpers} from 'quote-fetching';
const {intraday} = helpers;

export const intradayIndex = (timestamp, increment = 5) => {
  return intraday.intradayIndex(timestamp, increment);
}

export const ticksToEndOfDay = (timestamp, increment = 5) => {
  return intraday.ticksToEndOfDay(timestamp, increment);
}
