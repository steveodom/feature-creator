import {expect} from 'chai';
import Promise from 'bluebird';
import {details} from 'namer';
import {fetchAndHydrate, currentN} from './../src/index.js';
import {nockEndpoint} from './testHelper';
import {mockIndexDailyByDate, seriesDaily} from './src/relative-strength/sampleQuotes';
import _ from 'lodash';

describe('Features - getFeatures', (done) => {
  describe('with relative strength', () => {
    nockEndpoint('aapl', '1d');
    nockEndpoint('%5Egspc', '1d', 'index');

    const id = 'scrawny-boar';
    const namer = details(id, 'stocks');
    it('should return an array', () => {
      const features = fetchAndHydrate('aapl', '1d', 'forecast', namer, mockIndexDailyByDate);
      return features.then( (res) => { 
        expect(Array.isArray(res)).to.equal(false);
        expect(res).to.haveOwnProperty('Date');
        expect(res).to.haveOwnProperty('change');
        expect(res).to.haveOwnProperty('rsi-relativePct-5');
      });
    });
  });

  describe('with relative strength in recipe but no indexSeries passed in', () => {
    nockEndpoint('aapl', '1d');
    nockEndpoint('%5Egspc', '1d', 'index');

    const id = 'scrawny-boar';
    const namer = details(id, 'stocks');
    it('should throw an error', () => {
      const features = fetchAndHydrate('aapl', '1d', 'forecast', namer, {});
      return features.then( (res) => { 
        expect(Array.isArray(res)).to.equal(false);    
      }).catch( (err) => {
        expect(err).to.equal('a valid indexSeries which is an object with timeindex as keys must be passed in');
      });
    });
  });


  describe('with standard recipe', () => {
    nockEndpoint('aapl', '1d');
    const id = 'testing';
    const namer = details(id, 'stocks');
    it('should return an array of quotes', () => {
      const fnc = fetchAndHydrate('aapl', '1d', 'forecast', namer, []);
      return fnc.then( (res) => { 
        expect(Array.isArray(res)).to.equal(false);
        expect(res).to.haveOwnProperty('Date');
        expect(res).to.haveOwnProperty('change');
        done;
      });
    });
  });
});

describe('Index - currentN', () => {
  const res = currentN(seriesDaily, 4);
  it('should return an object containing the most recent average trueRange, n, nPct', () => { 
    expect(typeof res).to.equal('object');
    expect(res).to.haveOwnProperty('tr');
    expect(res).to.haveOwnProperty('n');
    expect(res).to.haveOwnProperty('nPct');
    expect(res).to.haveOwnProperty('twoStdDevPctChange');
  });

  it('should calculate nPct as n as a pct of the last quote', () => { 
    const n = res.n;
    const pct = _.round(n / 132.42, 5);
    expect(n).to.equal(1.4055);
    expect(res.nPct).to.equal(pct)
  });
});