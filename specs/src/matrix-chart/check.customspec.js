import {expect} from 'chai';
import MatrixChartMethods from './../../../src/matrix-chart/methods';
import MXCalcs from './../../../src/matrix-chart/calcs';
import getQuotes from 'quote-fetching';

// manual test to see matrix chart output
// describe('MatrixChartMethods - non mocked checker', () => {
  
//   it('should return an array of strings that match the last four series by low to high order', () => {
//     return getQuotes('AMZN', '1d').then( (quotes) => {
//       const closes = quotes.series.map( (quote) => (quote.close));
//       const period = 1;
//       const calcs = new MXCalcs(closes, 4, 'str');
//       let i = closes.length - 1;
//       let res = new MatrixChartMethods([], calcs).stringChart(period, i);
//       console.log(closes.slice(i-3), res, i, quotes.series[0]);
//       expect(Array.isArray(res)).to.equal(false);
//       // highest segment should be oool.
//       // lowest segment should be looo.
//       return expect(res).to.equal('oloo-looo-oool-looo');  
//     })
//   });
// });

describe('MatrixChartMethods - volume related non mocked checker', () => {
  
  it('should return an array of strings that match the last four series by low to high order', () => {
    return getQuotes('S', '1d').then( (quotes) => {
      const volumes = quotes.series.map( (quote) => (quote.volume));
      const period = 5;
      const useAverageRange = true;
      const calcs = new MXCalcs(volumes, 4, 'str', useAverageRange);
      let i = volumes.length - 1;
      let res = new MatrixChartMethods([], calcs).stringChart(period, i);
      console.log(volumes.slice(i-19), res, i, quotes.series[i]);
      expect(Array.isArray(res)).to.equal(false);
      // highest segment should be oool.
      // lowest segment should be looo.
      return expect(res).to.equal('looo-oolo-oool-oloo');  
    })
  });
});