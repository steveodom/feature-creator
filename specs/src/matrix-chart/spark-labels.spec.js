import {expect} from 'chai';

import SparkLabels from './../../../src/matrix-chart/spark-labels';

describe('SparkLabels - bins', () => {

  it('should return an array equal to the number of bins', () => {
    let ary = [45,42,44,44,45,47,48,50];
    let bins = 2;
    let mc = new SparkLabels(ary, bins);
    let res = mc.columns()
    expect(Array.isArray(res)).to.be.ok;
    expect(res.length).to.equal(bins);
  });

  it('should return an array where the smallest element in the array is equal to 10000000', () => {
    let ary = [45,42,44,44,45,47,48,50];
    let bins = 8;
    let mc = new SparkLabels(ary, bins);
    let res = mc.columns();
    expect(res[1]).to.equal('10000000');
  });

  it('should return an array where the largest element in the array is equal to 00000001', () => {
    let ary = [45,42,44,44,45,47,48,50];
    let bins = 8;
    let mc = new SparkLabels(ary, bins);
    let res = mc.columns();

    expect(res[6]).to.equal('00000010');
    expect(res[7]).to.equal('00000001');
  });

  it('should return an array of words with labelType === words passed in', () => {
    let ary = [45,42,44,44,45,47,48,50];
    let bins = 8;
    let mc = new SparkLabels(ary, bins, 'str');
    let res = mc.columns();

    expect(res[6]).to.equal('oooooolo');
    expect(res[7]).to.equal('oooooool');
  });
});


describe('SparkLabels labels - to return an array of numbers like like 1000, 0100', () => {
  let bins = 8;
  let ary = [ 50, 48, 47, 45, 44, 44, 42, 45 ];
  let mc = new SparkLabels(ary, bins);
  let res = mc.labels();

  it('should return an array of the right length', () => {
    expect(res.length).to.equal(bins);
  });

  it('should have 1s in the right place', () => {
    expect(res[0]).to.equal('10000000');
    expect(res[7]).to.equal('00000001');
  });
});


describe('SparkLabels labels - to return an array of numbers like like 1000, 0100 with length of each element equal to the bin', () => {
  // bins is equal to number of elements returned as well as the number of columns [10, 00] 
  let bins = 4;
  let ary = [ 50, 48, 47, 45, 44, 44, 42, 45 ];
  let mc = new SparkLabels(ary, bins);
  let res = mc.labels();

  it('should return an array of the right length', () => {
    expect(res.length).to.equal(bins);
  });

  it('should have 1s in the right place', () => {
    expect(res[0]).to.equal('1000');
    expect(res[3]).to.equal('0001');
  });
});

describe('SparkLabels columns - str argument.', () => { 
  let bins = 4;
  let ary = [ 50, 48, 47, 45, 44, 44, 42, 45 ];
  let mc = new SparkLabels(ary, bins, 'str');
  let res = mc.columns();

  it('should have ls and 0s in the right place', () => {
    expect(res[0]).to.equal('oool');
    expect(res[3]).to.equal('looo');
  });
});

describe('SparkLabels colunn - useAverageRangeArgument.', () => { 
  let bins = 4;
  let ary = [ 60, 48, 47, 45, 44, 44, 42, 25 ];
  let mc = new SparkLabels(ary, bins, 'str', false);
  let res = mc.columns();
  let mcAvg = new SparkLabels(ary, bins, 'str', true);
  let resAvg = mcAvg.columns();

  it('should return different results for different values of useAverageRangeArgment', () => {
    expect(res).to.deep.equal([ 'oool', 'oolo', 'oolo', 'looo' ]);
    expect(resAvg).to.deep.equal([ 'oool', 'oolo', 'oolo', 'looo' ]);
  });
});

describe('SparkLabels labels - real data. Amzn', () => {
  let bins = 8;
  let ary = [ 924.99,925,925.17,924.68,924.89,926.29,926.0935,925.49 ];
  let mc = new SparkLabels(ary, bins, 'str');
  let res = mc.columns();

  it('should return an array of the right length', () => {
    console.log(res, 'res');
    expect(res.length).to.equal(bins);
  });

  it('should have 1s in the right place', () => {
    expect(res[0]).to.equal('oloooooo');
    expect(res[7]).to.equal('oooolooo');
  });
});
