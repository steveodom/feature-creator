import {expect} from 'chai';
import MatrixChart from './../../../src/matrix-chart';

import {defaultSeries, defaultRecipe} from './../../../src/sample-data';


describe('MatrixChart - init', () => {

  it('should add methods to each recipe', () => {
    const b = new MatrixChart(defaultSeries, defaultRecipe);
    b.init();
    const res = b.recipe.matrixChart[0];
    expect(res).to.have.property('methods');
  });
});


describe('MatrixChart - build', () => {

  it('should return an object with keys from the basic recipe', () => {
    const b = new MatrixChart(defaultSeries, defaultRecipe);
    b.init();

    let res = b.build(defaultSeries[0], 0);
    expect(typeof res).to.equal('object');
    expect(res).to.have.property('mxc-5');
    expect(res).to.have.property('mxv-20');
  });
});

describe('MatrixChart - post', () => {

  it('should return an object with keys from the basic recipe', () => {
    const b = new MatrixChart(defaultSeries, defaultRecipe);
    b.init();

    let feature = b.build(defaultSeries[0], 0);
    const res = b.post(feature);
    expect(typeof res).to.equal('object');
    expect(res).to.have.property('mxc-5-isFlatPattern');
  });

  it('should return an object if matrixChartPost does not exist', () => {
    const modifiedRecipe = {
      basic: ['closeAtHigh', 'closeAtLow'],
      matrixChart: [
      {feature: 'stringChart', name: 'mx5', 'period': 5, type: 'closes', granularity: 8}
      ]
    }

    const b = new MatrixChart(defaultSeries, modifiedRecipe);
    b.init();

    let feature = b.build(defaultSeries[0], 0);
    const res = b.post(feature);
    expect(res).to.not.have.property('mxc-5-isFlatPattern');
  });

  it('should return hydrated object if matrixChartPost exist', () => {
    const modifiedRecipe = {
      basic: ['closeAtHigh', 'closeAtLow'],
      matrixChart: [
        {feature: 'stringChart', name: 'mx5', 'period': 5, type: 'closes', granularity: 4}
      ],
      matrixChartPost: [
        {feature: 'isFlatPattern', 'period': 5, type: 'closes'}
      ]
    }

    const b = new MatrixChart(defaultSeries, modifiedRecipe);
    b.init();

    let feature = b.build(defaultSeries[0], 4);
    const res = b.post(feature);
    expect(res).to.have.property('mxc-5-isFlatPattern');
    expect(res['mxc-5-isFlatPattern']).to.equal(true);
  });
});
