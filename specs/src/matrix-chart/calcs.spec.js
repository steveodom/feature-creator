// import {expect} from 'chai';
// import { ticks } from 'd3-array';

// import MatrixChart from './../../lib/utils/matrix-chart';


// describe('calcMatrixForWindow', () => {
//   describe('basic', () => {

//     let ary = ticks(1,40,40);

//     let mc = new MatrixChart(ary);
//     let startIndex = 40;
//     let goBackSegments = 4;
//     let {start, l, target, chunks} = mc.calcMatrixForWindow(startIndex, goBackSegments);

//     it('should take an ary as argument to MatrixChart', () => {
//       expect(ary.length).to.equal(40);
//       expect(Array.isArray(ary)).to.be.ok;
//       expect(ary[0]).to.equal(1);
//       expect(ary[39]).to.equal(40);
//     });

//     it('should return the starting index of our window', () => {
//       // the window ends at 40 and goes back 1
//       expect(start).to.equal(startIndex - (goBackSegments * 4));
//       expect(start).to.equal(24);
//     });

//     it('should return l to equal to the number of days we want in total', () => {
//       expect(l).to.equal(goBackSegments * 4);
//       expect(l).to.equal(16);
//     });

//     it('should return start + l to equal our i element', () => {
//       let finIndex = start + l;
//       expect(finIndex - 1).to.equal(39);
//       expect(ary[finIndex - 1]).to.equal(40);
//     });

//     it('should return target as the window we want', () => {
//       expect(target.length).to.equal(goBackSegments * 4);
//       expect(target[0]).to.equal(25);
//       expect(target[target.length - 1]).to.equal(40);
//     });

//     it('should return 4 chunks', () => {
//       expect(chunks.length).to.equal(4);
//     })

//     it('should return chunks of elements 4 elements long', () =>{
//       expect(chunks[0]).to.deep.equal([ 25, 26, 27, 28 ]);
//       expect(chunks[1]).to.deep.equal([ 29, 30, 31, 32 ]);
//       expect(chunks[3]).to.deep.equal([ 37, 38, 39, 40 ]);
//     })
//   });

//   describe('Longer example', () => {
//     let ary = ticks(1,40,40);

//     let startIndex = 40;
//     let goBackSegments = 6;

//     let mc = new MatrixChart(ary);
//     let {start, l, target} = mc.calcMatrixForWindow(startIndex, goBackSegments);

//     it('should return a longer length', () => {
//       expect(l).to.equal(goBackSegments * 4);
//     });

//     it('should an earlier start index', () => {
//       expect(start).to.equal(startIndex - (goBackSegments * 4));
//     });

//     it('should return longer target', () => {
//       expect(target.length).to.equal(goBackSegments * 4);
//     });


//   });

//   describe('Even longer window', () => {
//     let ary = ticks(1,40,40);

//     let startIndex = 40;
//     let goBackSegments = 14;

//     let mc = new MatrixChart(ary);
//     let {start, l, target} = mc.calcMatrixForWindow(startIndex, goBackSegments);

//     it('should return a length not longer than our whole array', () => {
//       expect(l).to.equal(startIndex);
//     });

//     it('should return a starting index of 0 so as not to exceed the start of our whole array', () => {
//       expect(start).to.equal(0);
//     });

//     it('should return the whole array as our target since our window is wider than our array', () => {
//       expect(target.length).to.equal(ary.length);
//     });
//   });
// })


// it('MatrixChart - matrix ascending', () => {
//   let ary = ticks(1,40,40);

//   let mc = new MatrixChart(ary);
//   let { matrix } = mc.calcMatrixForWindow(40, 14);

//   // console.info(chunks, matrix);
//   expect(Array.isArray(matrix)).to.be.ok;
//   expect(matrix.length).to.equal(4);

//   expect(matrix[0]).to.deep.equal([1,0,0,0], '-- the highest should be first since we reversed');
// });

// it('MatrixChart - matrix descending', () => {
//   let ary = ticks(1,40,40);

//   let mc = new MatrixChart(ary.reverse());
//   let {matrix} = mc.calcMatrixForWindow(40, 14);

//   expect(matrix[0]).to.deep.equal([0,0,0,1], '-- the highest should be first since we reversed');
// });

// it('MatrixChart - yahoo dec 1 2005 back 5 days', () => {
//   let ary = [
//     51.72,
//     51.93,
//     51.74,
//     50.1
//   ];

//   let mc = new MatrixChart(ary);

//   let {matrix} = mc.calcMatrixForWindow(4, 1);
//   // console.info(chunks, matrix);
//   expect(matrix[0]).to.deep.equal([0,0,0,1], '-- the highest should be first since we reversed');
//   expect(matrix).to.deep.equal([[ 0, 0, 0, 1 ], [ 0, 0, 0, 1 ], [ 0, 0, 0, 1 ], [ 1, 0, 0, 0 ]]);
// });

// it('MatrixChart - matrix flat', () => {
//   let ary = [];
//   let n = 0;
//   while (n < 40) {
//     ary.push(40);
//     n++;
//   }

//   let mc = new MatrixChart(ary);
//   let { matrix} = mc.calcMatrixForWindow(40, 4);

//   expect(matrix[0]).to.deep.equal([0,0,0,1], '-- should be flat');
//   expect(matrix[1]).to.deep.equal([0,0,0,1], '-- should be flat');
//   expect(matrix[3]).to.deep.equal([0,0,0,1], '-- should be flat');
// });


// describe('to256', () => {
//   it('converts matrix strings to a 256 num', () => {
//     let mc = new MatrixChart([]);
//     let res = mc.to256(1);
//     expect(res).to.equal(0);

//     res = mc.to256(10);
//     expect(res).to.equal(85);

//     res = mc.to256(100);
//     expect(res).to.equal(171);

//     res = mc.to256(1000);
//     expect(res).to.equal(256)
//   });
// })


// describe('avgOfColumns', () => {
//   let mc = new MatrixChart([]);
//     let arrays = [
//       [1,0,0,0],
//       [0,1,0,0],
//       [0,0,1,0],
//       [0,0,0,1],
//     ];

//   it('should be the lowest number for a descending example. The lower the number, the darker the color', () => {
//     let res = mc.avgOfColumns(arrays);
//     expect(res).to.equal(85);
//   });

//   it('should be a higher number for an ascending example', () => {
//     let res = mc.avgOfColumns(arrays.reverse());
//     expect(res).to.equal(171);
//   });

//   it('should be a in between number for flat example', () => {
//     arrays = [
//       [0,1,0,0],
//       [0,1,0,0],
//       [0,0,1,0],
//       [0,1,0,0],
//     ];
//     let res = mc.avgOfColumns(arrays);
//     expect(res).to.equal(145);
//   });

//   it('should be a higher number/darker color for flat then down example', () => {
//     arrays = [
//       [0,0,1,0],
//       [0,0,1,0],
//       [0,0,1,0],
//       [1,0,0,0],
//     ];
//     let res = mc.avgOfColumns(arrays);
//     expect(res).to.equal(153);
//   });

// });

// describe('arrayToText', () => {
//   let mc = new MatrixChart([]);
//   it('should return a letter', () => {
//     let res = mc.arrayToText([0,0,1,0], 0);
//     expect(res).to.equal('h');
//   });

//   it('should return a letter', () => {
//     let res = mc.arrayToText([1,0,0,0], 0);
//     expect(res).to.equal('c');
//   })
// })

// describe('columnsToText', () => {
//   let mc = new MatrixChart([]);
//   it('should return a string (gibberish word)', () => {
//     let arrays = [
//       [0,0,1,0],
//       [0,0,1,0],
//       [0,0,1,0],
//       [1,0,0,0],
//     ];
//     let res = mc.columnsToText(arrays, 'str');
//     expect(res).to.equal('houk');
//   });

//   it('should return a string (gibberish word)', () => {
//     let arrays = [
//       [1,0,0,0],
//       [0,1,0,0],
//       [0,0,1,0],
//       [0,0,0,1],
//     ];
//     let res = mc.columnsToText(arrays, 'str');
//     expect(res).to.equal('ceun');
//   });
// })
