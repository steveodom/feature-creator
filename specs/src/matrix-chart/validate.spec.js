import {expect} from 'chai';

import validateMatrixChart from './../../../src/matrix-chart/validate';
import {defaultRecipe} from './../../../src/sample-data';

describe('validateMatrixChart', () => {

  it('should return true if all recipes have valid methods', () => {
    const res = validateMatrixChart(defaultRecipe.matrixChart);
    expect(res.isValid).to.equal(true);
    expect(res.msg.length).to.equal(0);
  });

  it('should return false if feature not seen', () => {
    const modifiedRecipe = [
      {feature: 'diffChart', name: 'mx5', 'period': 5, type: 'closes', granularity: 8},
    ];

    const res = validateMatrixChart(modifiedRecipe);
    expect(res.isValid).to.equal(false);
    expect(res.msg.length).to.equal(1);
    expect(res.msg[0]).to.equal('Specified MC feature was not found: diffChart');
  });
});
