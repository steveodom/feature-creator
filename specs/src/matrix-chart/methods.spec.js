import {expect} from 'chai';
import MatrixChartMethods from './../../../src/matrix-chart/methods';
import MXCalcs from './../../../src/matrix-chart/calcs';
const series = [
  {close: 5.5},
  {close: 5.5},
  {close: 5.0},
  {close: 5.5},
  {close: 5.5},
  {close: 5.5},
  {close: 5.5},
  {close: 5.5},
  {close: 5.5},
  {close: 5.5},
  {close: 4.5},
  {close: 4.0},
  {close: 5.5}
];

describe('MatrixChartMethods - chart', () => {
  const closes = series.map( (quote) => (quote.close));
  const period = 1;
  const calcs = new MXCalcs(closes, 8, 'str');

  it('should return an array of strings', () => {
    let i = 0;
    let res = new MatrixChartMethods(series, calcs).chart(period, i);
    expect(Array.isArray(res)).to.equal(true);
    expect(res[0]).to.equal('oooooool');
  });

  it('should return a length of 1 for the very first result since it does not have any history', () => {
    let i = 0;
    let res = new MatrixChartMethods(series, calcs).chart(period, i);
    expect(res.length).to.equal(1);
  });

  it('should return a length of 2 for the second result. Both represent tops', () => {
    let i = 1;
    let res = new MatrixChartMethods(series, calcs).chart(period, i);
    expect(res.length).to.equal(2);
    expect(res[0]).to.equal('oooooool');
    expect(res[1]).to.equal('oooooool');
  });

  it('should return a length of 3 for the second result. First two represent tops. Third represent bottom since it goes down to its lowest point', () => {
    let i = 2;
    let res = new MatrixChartMethods(series, calcs).chart(period, i);
    expect(res.length).to.equal(3);
    expect(res[0]).to.equal('oooooool');
    expect(res[1]).to.equal('oooooool');
    expect(res[2]).to.equal('looooooo');
  });

  it('should return a length of 8 for the last result', () => {
    let i = 12;
    let res = new MatrixChartMethods(series, calcs).chart(period, i);
    expect(res.length).to.equal(8);
    expect(res[5]).to.equal('oolooooo');
    expect(res[6]).to.equal('looooooo');
    expect(res[7]).to.equal('oooooool');  
  });
});

describe('MatrixChartMethods - chart with alt text rendering', () => {
  const closes = series.map( (quote) => (quote.close));
  const period = 1;

  // set the output here
  const calcs = new MXCalcs(closes, 8, 'strAlt');

  it('should return an array of strings', () => {
    let i = 0;
    let res = new MatrixChartMethods(series, calcs).chart(period, i);
    expect(Array.isArray(res)).to.equal(true);
    expect(res[0]).to.equal('dddddddu');
  });
});


describe('MatrixChartMethods - stringChart', () => {

  const closes = series.map( (quote) => (quote.close));
  const period = 1;
  const calcs = new MXCalcs(closes, 8, 'str');
  
  it('should return an array of strings', () => {
    let i = 0;
    let res = new MatrixChartMethods(series, calcs).stringChart(period, i);
    expect(Array.isArray(res)).to.equal(false);
    expect(typeof res).to.equal('string');
    expect(res).to.equal('oooooool');
  });

  it('should return a 2 segments joined by a - for the second chart', () => {
    let i = 1;
    let res = new MatrixChartMethods(series, calcs).stringChart(period, i);
    expect(res).to.equal('oooooool-oooooool');
  });

  it('should return a - segmented string for 12 position', () => {
    let i = 12;
    let res = new MatrixChartMethods(series, calcs).stringChart(period, i);
    expect(res).to.equal('oooooool-oooooool-oooooool-oooooool-oooooool-oolooooo-looooooo-oooooool');
  });
});

describe('Matrix-Chart - isFlatPattern', () => {
  const closes = series.map( (quote) => (quote.close));
  const period = 1;
  const calcs = {}

  it('should return false if not in right format', () => {
    const feature = { 'mxc-5': ['looo','looo', 'oool'] }
    let res = new MatrixChartMethods(series, calcs).isFlatPattern(feature['mxc-5']);
    expect(res).to.equal(false);
  });

  it('should return false if not in right format', () => {
    const feature = { 'mxc-5': 'looolooooool' }
    let res = new MatrixChartMethods(series, calcs).isFlatPattern(feature['mxc-5']);
    expect(res).to.equal(false);
  });

  it('should return true if flatPattern', () => {
    const feature = { 'mxc-5': 'looo-oloo-looo-oool' }
    let res = new MatrixChartMethods(series, calcs).isFlatPattern(feature['mxc-5']);
    expect(res).to.equal(true);
  });

  it('should return true if flatPattern', () => {
    const feature = { 'mxc-5': 'oool-oloo-looo-oool' }
    let res = new MatrixChartMethods(series, calcs).isFlatPattern(feature['mxc-5']);
    expect(res).to.equal(true);
  });
});

describe('Matrix-Chart - isUpOrDownPattern', () => {
  const closes = series.map( (quote) => (quote.close));
  const period = 1;
  const calcs = {}

  it('should return false if not in right format', () => {
    const feature = { 'mxc-5': ['looo','looo', 'oool'] }
    let res = new MatrixChartMethods(series, calcs).isUpOrDownPattern(feature['mxc-5']);
    expect(res).to.equal(false);
  });

  it('should return false if not in right format', () => {
    const feature = { 'mxc-5': 'looolooooool' }
    let res = new MatrixChartMethods(series, calcs).isUpOrDownPattern(feature['mxc-5']);
    expect(res).to.equal(false);
  });

  it('should return true if UpOrDownPattern', () => {
    const feature = { 'mxc-5': 'oloo-oloo-looo-oolo' }
    let res = new MatrixChartMethods(series, calcs).isUpOrDownPattern(feature['mxc-5']);
    expect(res).to.equal(true);
  });

  it('should return false if not UpOrDownPattern', () => {
    const feature = { 'mxc-5': 'oool-oolo-looo-oloo' }
    let res = new MatrixChartMethods(series, calcs).isUpOrDownPattern(feature['mxc-5']);
    expect(res).to.equal(false);
  });
});

describe('Matrix-Chart - isBelowToAbovePattern', () => {
  const closes = series.map( (quote) => (quote.close));
  const period = 1;
  const calcs = {}

  it('should return true if isBelowToAbovePattern', () => {
    const feature = { 'mxc-5': 'oolo-oolo-looo-oloo' };
    let res = new MatrixChartMethods(series, calcs).isBelowToAbovePattern(feature['mxc-5']);
    expect(res).to.equal(true);
  });

  it('should return true if right pattern', () => {
    const feature = { 'mxc-5': 'oool-oolo-looo-oloo' }
    let res = new MatrixChartMethods(series, calcs).isBelowToAbovePattern(feature['mxc-5']);
    expect(res).to.equal(true);
  });
});