import {expect} from 'chai';

import validator from './../../../src/validator';
import {defaultRecipe} from './../../../src/sample-data';
import {mockIndexDaily} from './../relative-strength/sampleQuotes';

describe('validator', () => {

  it('should return an object with isValid property', () => {
    const res = validator(defaultRecipe, mockIndexDaily.series);
    expect(res.isValid).to.equal(true);
    expect(res.msg.length).to.equal(0);
  });

  it('should return isValid false if feature not seen', () => {
    const modifiedRecipe = Object.assign({}, defaultRecipe, {
      matrixChart: [
        {feature: 'chartNope', name: 'mx5', 'period': 5, type: 'closes', granularity: 8}
      ]
    });

    let res = validator(modifiedRecipe, mockIndexDaily.series);
    expect(res.isValid).to.equal(false);
    expect(res.msg.length).to.equal(1);
    expect(res.msg[0]).to.equal('Specified MC feature was not found: chartNope');
  });

  it('should return isValid false if change is not seen', () => {
    const modifiedRecipe = {
      basic: ['closeAtHigh', 'closeAtLow']
    }

    let res = validator(modifiedRecipe, mockIndexDaily.series);
    expect(res.isValid).to.equal(false);
    expect(res.msg.length).to.equal(3);
    expect(res.msg[0]).to.equal('A calculation method is required');
    expect(res.msg[1]).to.equal('A period n is require to look foward');
    expect(res.msg[2]).to.equal('A threshold1 is required to know how much to measure');
  });

  it('should be able to handle a missing component of a recipe eg. no matrixChart being included', () => {
    const modifiedRecipe = {
      basic: ['closeAtHigh', 'closeAtLow'],
      movingAverage: [
        {feature: 'movingAverage', name: 'ma5', 'period': 5, type: 'rolling'},
        {feature: 'closeAboveAvg', name: 'above5Avg', 'period': 5, type: 'rolling'},
        {feature: 'daysAgoChange', name: 'daysAgo5Change', 'period': 5, type: 'rolling', daysAgo: 5}
      ],
      changeCalc: {
        method: 'isGreaterThan',
        n: 1,
        threshold1: 1,
        threshold2: -0.5
      }
    }

    let res = validator(modifiedRecipe);
    expect(res.isValid).to.equal(true);
  });
});
