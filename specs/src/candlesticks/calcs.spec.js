import {expect} from 'chai';
import CSCalcs from './../../../src/candlesticks/calcs';
import {longerSeries} from './../../../src/sample-data';

describe('Candlestick Calcs - Abandoned Baby', () => {
  let cs = new CSCalcs(longerSeries);
  let daysAgo = 2;
  const res = cs.inputs(daysAgo);

  it('should return an array of objects', () => {  
    expect(Array.isArray(res)).to.equal(true);
    const mostRecent = res[res.length - 1];

    describe('each element is an object', () => {
      it('should have close, high, low, and open properties', () => {
        expect(mostRecent).to.haveOwnProperty('close');
        expect(mostRecent).to.haveOwnProperty('high');
        expect(mostRecent).to.haveOwnProperty('low');
        expect(mostRecent).to.haveOwnProperty('open');
        console.log(mostRecent, 'mostRecent', longerSeries);
        
        describe('each property is an array ', () => {
          it('should be the length of the number of days passed in', () => {
            expect(mostRecent.close.length).to.equal(daysAgo);
          });

          it('should contain the correct elements', () => {
            const closes = longerSeries.map( (obj) => (obj.close));
            const target = closes.slice(-1 * daysAgo); // -2. gives the last two;
            expect(mostRecent.close).to.deep.equal(target);
          })
        });
      });
    });
  });
});
