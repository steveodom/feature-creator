import {expect} from 'chai';
import _ from 'lodash';
import CsMethods from './../../../src/candlesticks/methods';
import CsCalcs from './../../../src/candlesticks/calcs';
import {longerSeries} from './../../../src/sample-data';


const calcs = new CsCalcs(longerSeries);
const daysAgo = 2;
const inputs = calcs.inputs(daysAgo);
const cs = new CsMethods(longerSeries, inputs);

describe('Candlesticks Methods - abandonedBaby', () => {  

  it('should return a boolean', () => {
    const i = 1;
    const res = cs.abandonedBaby({}, i);
    expect(typeof res).to.equal('boolean');
    expect(res).to.equal(false);
  });
});
