import {expect} from 'chai';

import RecipeGroup from './../../../src/recipeGroups';
import {defaultRecipe} from './../../../src/sample-data';

describe('recipeGroups', () => {

  it('should return an object with normalize, quantile & ngram properties', () => {
    const res = new RecipeGroup().build(defaultRecipe);
    expect(res).to.have.property('normalize');
    expect(res).to.have.property('quantile');
    expect(res).to.have.property('ngram');
  });

  describe('normalize', () => {
    const res = new RecipeGroup().build(defaultRecipe);
    it('should start with group', () => {
      expect(res.normalize).to.include('group(');
    });

    it('should contain close', () => {
      expect(res.normalize).to.include('close');
    });

    it('should contain movingAverage', () => {
      expect(res.normalize).to.include('mar-movingAverage');
    });     

    it('should contain supportResistance', () => {
      expect(res.normalize).to.include('sr-supportLevel-236-5');
    });
  });

  describe('quantile', () => {
    const res = new RecipeGroup().build(defaultRecipe);
    it('should start with group', () => {
      expect(res.quantile).to.include('group(');
    });

    it('should contain daysAgoChange', () => {
      expect(res.quantile).to.include('mar-daysAgoChange');
    });

    it('should contain rsi-relativePct-20', () => {
      expect(res.quantile).to.include('rsi-relativePct-20');
    });
  });

  describe('ngram', () => {
    it('should start with group', () => {
      const res = new RecipeGroup().build(defaultRecipe);
      expect(res.ngram).to.include('group(');
    });

    it('should contain mxc-5', () => {
      const res = new RecipeGroup().build(defaultRecipe);
      expect(res.ngram).to.include('mxc-5');
    });
  });

});
