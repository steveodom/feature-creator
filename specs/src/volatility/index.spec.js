import {expect} from 'chai';
import VO from './../../../src/volatility';

import {defaultSeries, defaultRecipe} from './../../../src/sample-data';


describe('Volatility - init', () => {

  it('should NOT add methods to default recipe since volatility does not exist', () => {
    const b = new VO(defaultSeries, defaultRecipe);
    b.init();
    const res = b.recipe.volatility[0];
    expect(res).to.be.undefined;
  });

  it('should NOT add methods to default recipe since volatility does not exist', () => {
    const modifiedRecipe = Object.assign({}, defaultRecipe, {
      volatility: [{feature: 'ratio', 'period': 20 }]
    })
    const b = new VO(defaultSeries, modifiedRecipe);
    b.init();
    const res = b.recipe.volatility[0];
    expect(res).to.haveOwnProperty('methods');
  });
});


describe('Volatility - build', () => {

  it('should return an object with keys from the basic recipe', () => {
    const modifiedRecipe = Object.assign({}, defaultRecipe, {
      volatility: [{feature: 'ratio', 'period': 20 }]
    })
    const b = new VO(defaultSeries, modifiedRecipe);
    b.init();

    let res = b.build(defaultSeries[0], 0);
    expect(typeof res).to.equal('object');
    expect(res).to.haveOwnProperty('vo-ratio-20');
  });
});