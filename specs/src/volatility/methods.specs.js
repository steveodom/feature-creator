import {expect} from 'chai';
import _ from 'lodash';
import VoMethods from './../../../src/volatility/methods';
import VoCalcs from './../../../src/volatility/calcs';
import {longerSeries} from './../../../src/sample-data';


const calcs = new VoCalcs(longerSeries);
const highs = calcs.rolling(2, 'high');
const lows = calcs.rolling(2, 'low');
const vo = new VoMethods(longerSeries, highs, lows);

describe('Volatility Methods - trueRange', () => {  

  it('should return a number which is diff between the average of the previous close and the current high and the same thing but with the low', () => {
    const i = 1;
    const res = vo.trueRange(longerSeries[i], i);
    expect(typeof res).to.equal('number');
    expect(res).to.equal(2.27);
  });

  it('should be able to handle missing data', () => {
    const i = 0;
    const res = vo.trueRange(longerSeries[i], i);
    expect(typeof res).to.equal('number');
    expect(res).to.equal(0);
  });
});

describe('Volatility Methods - prevTrueRange', () => {
  it('should return a number which is the diff between average of the previous highs window and the average of the lows', () => {
    const i = 1;
    const res = vo.prevTrueRange(i);
    const diff = 782.84 - 775.22;
    expect(typeof res).to.equal('number');
    expect(res).to.equal(_.round(diff,2));
    expect(res).to.equal(7.62);
  });

  it('should be able to handle missing data', () => {
    const i = 0;
    const res = vo.prevTrueRange(i);
    expect(typeof res).to.equal('number');
    expect(res).to.equal(10.7);
  });
});

describe('Volatility Methods - ratio', () => {
  const i = 1;
  const res = vo.ratio(longerSeries[i], i);

  it('should return a number which is the ratio of the trueRange / prevTruerange', () => {
    expect(typeof res).to.equal('number');
    expect(res).to.equal(_.round(2.27/7.62,2));
    expect(res).to.equal(0.3);
  });
});
