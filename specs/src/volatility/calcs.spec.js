import {expect} from 'chai';
import VoCalcs from './../../../src/volatility/calcs';
import {defaultSeries} from './../../../src/sample-data';

describe('Volatility Calcs - folling high', () => {
  let vo = new VoCalcs(defaultSeries);
  const res = vo.rolling(2, 'high');

  it('should return an array of numbers with the right average', () => {  
    expect(Array.isArray(res)).to.equal(true);
    expect(typeof res[0]).to.equal('number');
    expect(res[1]).to.equal(782.84);
    expect(res[2]).to.equal(785.79);
  });
});

describe('Volatility Calcs - folling low', () => {
  let vo = new VoCalcs(defaultSeries);
  const res = vo.rolling(2, 'low');

  it('should return an array of numbers with the right average', () => {  
    expect(res[1]).to.equal(775.22);
    expect(res[2]).to.equal(779.33);
  });

  
});
