import {expect} from 'chai';
import Change from './../../../src/change';

import {defaultSeries, defaultRecipe} from './../../../src/sample-data';

describe('change - start calculation', () => {

  it('should return an object', () => {
    const c = new Change(defaultSeries, defaultRecipe);
    c.init(defaultRecipe.movingAverage);
    let res = c.build({}, 0);
    expect(typeof res).to.equal('object');
    expect(res).to.have.property('change');
  });
});
