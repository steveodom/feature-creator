import {expect} from 'chai';
import ChangeMethods from './../../../src/change/methods';
import MovingAverages from './../../../src/moving-averages';
import {longerSeries, defaultRecipe} from './../../../src/sample-data';

const series = [
  {close: 0},
  {close: 1},
  {close: 2},
  {close: 3},
  {close: 4},
  {close: 5},
  {close: 6},
  {close: 7},
  {close: 8},
  {close: 9},
  {close: 10},
  {close: 11}
];

describe('ChangeMethods - isGreaterThan', () => {

  it('should return a boolean', () => {
    let res = new ChangeMethods(series, 1).isGreaterThan(1, 1);
    expect(typeof res).to.equal('boolean');
    expect(res).to.equal(true);
  });
});

describe('ChangeMethods - isLessThan', () => {

  it('should return a boolean', () => {
    let res = new ChangeMethods(series, 1).isLessThan(1, 1);
    expect(typeof res).to.equal('boolean');
    expect(res).to.equal(false);
  });
});


describe('ChangeMethods - assignRange', () => {

  it('should return a string', () => {
    let res = new ChangeMethods(series, 1).assignRange(1, 0.01, -0.01);
    expect(typeof res).to.equal('string');
    expect(res).to.equal('up');
  });
});

describe('ChangeMethods - assignQuad', () => {

  it('should return a string', () => {
    let res = new ChangeMethods(series, 1).assignQuad(1, 0.01, 0, -0.01);
    expect(typeof res).to.equal('string');
    expect(res).to.equal('up');
  });

  it('should return a up-flat if does not go up that much', () => {
    const res = new ChangeMethods(series, 2).assignQuad(1, 230, 0, -0.01);
    expect(typeof res).to.equal('string');
    expect(res).to.equal('up-flat');
  });

  it('should return a down-flat if its below the mid point', () => {
    const res = new ChangeMethods(series, 2).assignQuad(1, 230, 210, 190);
    expect(typeof res).to.equal('string');
    expect(res).to.equal('down-flat');
  });
});

describe('ChangeMethods - assignQuadDeviation', () => {
  it('should return a string', () => {
    // setup the moving average std deviations to pass in
    const ma = new MovingAverages(series, defaultRecipe, []);
    ma.init();
    
    let res = new ChangeMethods(series, 1, ma.recipe.movingAverage).assignQuadDeviation(1, 0.01, 0, -0.01);
    expect(typeof res).to.equal('string');
    expect(res).to.equal('down-flat');
  });

  it('should handle 0 index, returning down-flat since change in 0', () => {
    // setup the moving average std deviations to pass in
    const ma = new MovingAverages(series, defaultRecipe, []);
    ma.init();
    
    let res = new ChangeMethods(series, 1, ma.recipe.movingAverage).assignQuadDeviation(0, 0.01, 0, -0.01);
    expect(typeof res).to.equal('string');
    expect(res).to.equal('down-flat');
  });
});

describe('ChangeMethods - assignQuadDeviation using longerSeries', () => {
  const ma = new MovingAverages(longerSeries, defaultRecipe, []);
  ma.init();
  const change = new ChangeMethods(longerSeries, 1, ma.recipe.movingAverage);
  const high = 1;
  const mid = 0;
  const low = -1;
  let i = 0;

  it('should handle undefined as stdDev with a down-flat', () => {
    let res = change.assignQuadDeviation(i, high, mid, low);
    // std dev at 0 is undefined. Undefined returns a -0, which triggers the down-flat
    expect(typeof res).to.equal('string');
    expect(res).to.equal('down-flat');
  });

  it('should handle return up if % change is above 0.95 high. -- stdDev is 0.98', () => {
    i = 1;
    let res = change.assignQuadDeviation(i, 0.95, mid, low);
    expect(typeof res).to.equal('string');
    expect(res).to.equal('down-flat');
  });

  it('should handle 0.13 change return a up-flat - stdDev % is 0.0977.', () => {
    i = 2;
    let res = change.assignQuadDeviation(i, high, mid, low);
    expect(typeof res).to.equal('string');
    expect(res).to.equal('down-flat');
  });  

  it('should handle 0.13 change return a up-flat - stdDev % equal to 0.07826.', () => {
    i = 3;
    let res = change.assignQuadDeviation(i, high, mid, low);
    expect(typeof res).to.equal('string');
    expect(res).to.equal('down-flat');
  });  
});

describe('ChangeMethods - changeAsPctOfTwoStdDeviationMove using longerSeries', () => {
  const ma = new MovingAverages(longerSeries, defaultRecipe, []);
  ma.init();
  const change = new ChangeMethods(longerSeries, 1, ma.recipe.movingAverage);
  const high = 1;
  const mid = 0;
  const low = -1;
  let i = 3;

  it('should handle return a number', () => {
    let res = change.changeAsPctOfTwoStdDeviationMove(i);
    expect(typeof res).to.equal('number');
    expect(res).to.equal(0);
  })
})

// describe('ChangeMethods - methods can be called dynamically', () => {

//   it('should call the isGreaterThanMethod', () => {
//     let klass = new ChangeMethods(series, 1);
//     let res = klass.isGreaterThan(1, 101);
//     expect(typeof res).to.equal('boolean');
//     expect(res).to.equal(false);
//   });

//   it('should call the isGreaterThanMethod from a variable', () => {
//     let klass = new ChangeMethods(series, 1);
//     let method = 'isGreaterThan'
//     let res = klass[method](1, 101);
//     expect(typeof res).to.equal('boolean');
//     expect(res).to.equal(false);
//   });
// })

