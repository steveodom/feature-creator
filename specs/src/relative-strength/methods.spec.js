import {expect} from 'chai';
import _ from 'lodash';
import Promise from 'bluebird';
import RSMethods from './../../../src/relative-strength/methods';
import RSCalcs from './../../../src/relative-strength/calcs';
import {seriesDaily, seriesRealtime, mockIndexDaily, mockIndexDailyByDate} from './sampleQuotes';

import nock from 'nock';

nock('http://chartapi.finance.yahoo.com')
  .get('/instrument/1.0/%5Egspc/chartdata;type=quote;range=1m/json')
  .reply(200, `finance_charts_json_callback( { "meta" :\n {\n  "uri":"/instrument/1.0/^gspc/chartdata;type=quote;range=1m/json",\n  "ticker" : "^gspc",\n  "Company-Name" : "S&P 500",\n  "Exchange-Name" : "SNP",\n  "unit" : "DAY",\n  "timestamp" : "",\n  "first-trade" : "19500103",\n  "last-trade" : "20170213",\n  "currency" : "USD",\n  "previous_close_price" : 2274.6399\n }\n ,\n "Date" : {"min" :20170117,"max" :20170213}\n ,\n "labels" : [20170117,20170123,20170130,20170206,20170213 ]\n ,\n "ranges" : {"close" : {"min" :2263.6899,"max" :2328.2500 },"high" : {"min" :2271.7800,"max" :2331.5801 },"low" : {"min" :2257.0200,"max":2321.4199 },"open" : {"min" :2267.7800,"max" :2321.7200 },"volume" : {"min" :3109050112,"max" :4087450112 } }\n ,\n "series" : ${JSON.stringify(mockIndexDaily.series)}\n} )`
);


describe('RelativeStrengthMethods - getEndQuote', () => {
  const b = new RSMethods(seriesDaily, {}, '1m');
  const quote = seriesDaily[4];

  it('should return a quote the right number of ticks away', () => {    
    let daysAgo = 2;
    const res = b.getEndQuote(4, daysAgo);
    expect(typeof res).to.equal('object');
    expect(res.Date.toString()).to.equal('20170206');
  });

  it('should return a quote the right number of ticks away', () => {    
    let daysAgo = 3;
    const res = b.getEndQuote(4, daysAgo);
    expect(res.Date.toString()).to.equal('20170203');
  });
});

describe('RelativeStrengthMethods - getStockChange', () => {
  
  it('should return a number which is the right pct change', () => {
    const b = new RSMethods(seriesDaily, {}, '1m');
    const quote = seriesDaily[4];
    const endQuote = seriesDaily[2];
    
    const res = b.getStockChange(quote, endQuote);
    const manualChange = ((quote.close - endQuote.close) / endQuote.close) * 100;
    expect(typeof res).to.equal('number');
    expect(quote.close).to.equal(132.04);
    expect(endQuote.close).to.equal(130.29);
    expect(res).to.equal(_.round(manualChange,2));
    expect(res).to.equal(1.34);
  });

  it('should return a number which is the right pct change', () => {
    const b = new RSMethods(seriesDaily, {}, '1m');
    const quote = mockIndexDaily.series[4];
    const endQuote = mockIndexDaily.series[2];
    
    const res = b.getStockChange(quote, endQuote);
    expect(quote.close).to.equal(2294.6699);
    expect(endQuote.close).to.equal(2292.5601);
    expect(res).to.equal(0.09);
  });
});


describe('RelativeStrengthMethods - daysAgoChange Daily', () => {
  
  it('should return a number which is the stocks change / index change', () => {
    const recipe = {feature: 'relativePct', 'daysAgo': 2 };
    
    let b = new RSMethods(seriesDaily, {}, mockIndexDailyByDate);
    let quote = seriesDaily[4];
    
    let res = b.relativePct(quote, 4, recipe);
    const manualChange = _.round( 1.34 / 0.09, 2);
    expect(typeof res).to.equal('number');
    expect(res).to.equal(manualChange);
  });
});

describe('RelativeStrengthMethods - rsi', () => {
  it('should return a number which is the relative strength for that day', () => {
    const recipe = {feature: 'rsi', 'daysAgo': 2 };
    const series = [...seriesRealtime, ...seriesRealtime];
    const averages = new RSCalcs(series).rsiList(recipe.daysAgo);
    let b = new RSMethods(series, averages, mockIndexDailyByDate);
    let quote = seriesRealtime[4];
    
    let res = b.rsi(quote, 1);
    let res2 = b.rsi(quote, 6);
    let res3 = b.rsi(quote, averages.length - 1);
    expect(typeof res).to.equal('number');
    expect(res).to.equal(0);
    expect(res2).to.equal(13.02);
    expect(res3).to.equal(90.43);
  });
});