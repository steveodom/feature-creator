import {expect} from 'chai';
import RelativeStrength from './../../../src/relative-strength';

import {defaultSeries, defaultRecipe} from './../../../src/sample-data';
import {mockIndexDailyByDate, mockIndexRealtimeByDate} from './sampleQuotes';

describe('RelativeStrength - init', () => {

  it('should add methods to each recipe', () => {
    const b = new RelativeStrength(defaultSeries, defaultRecipe, '1d');
    b.init();
    const res = b.recipe.relativeStrength[0];
    expect(res).to.have.property('methods');
  });
});


describe('RelativeStrength - build', () => {

  it('should return an empty object if empty index is passed in', () => {
    const b = new RelativeStrength(defaultSeries, defaultRecipe, []);
    b.init();

    let res = b.build(defaultSeries[0], 0);
    expect(typeof res).to.equal('object');
    expect(res).to.not.have.property('rsi-relativePct-20');
  });

  it('should return an object with keys from the basic recipe', () => {
    const b = new RelativeStrength(defaultSeries, defaultRecipe, mockIndexRealtimeByDate);
    b.init();

    let res = b.build(defaultSeries[0], 0);
    expect(typeof res).to.equal('object');
    expect(res).to.have.property('rsi-relativePct-20');
    expect(res).to.have.property('rsi-relativePct-5');
    expect(res).to.have.property('rsi-rsi-14');
  });
});
