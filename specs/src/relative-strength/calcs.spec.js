import {expect} from 'chai';
import Promise from 'bluebird';
import RSCalcs, {getIndexByDates} from './../../../src/relative-strength/calcs';
import fetch, {fetchDateBased} from 'quote-fetching';
import {seriesDaily, seriesRealtime, mockIndexDailyByDate, mockIndexRealtimeByDate} from './sampleQuotes';
import moment from 'moment';

describe('RelativeStrength Calcs - getIndexByDates Daily', () => {
  
  it('should return an object with two properties, which are quotes from the right day', () => {  
    let endQuote = seriesDaily[2];
    let startQuote = seriesDaily[4];
    let period = '1m'; //daily
    let index = mockIndexDailyByDate;

    const res = getIndexByDates(startQuote, endQuote, index);
  
    expect(typeof res).to.equal('object');
    expect(res).to.haveOwnProperty('startQuoteIndex');
    expect(res).to.haveOwnProperty('endQuoteIndex');
      
    describe('startQuoteIndex', () => {
      const target = res.startQuoteIndex;
      it('should be an index quote', () => {
        expect(target).to.haveOwnProperty('close');
        expect(target).to.haveOwnProperty('Date');
      });

      it('should be the correct date', () => {
        expect(target.Date.toString()).to.equal('20170208');
      });
    });

    describe('endQuoteIndex', () => {
      const target = res.endQuoteIndex;
      it('should be an index quote', () => {
        expect(target).to.haveOwnProperty('close');
        expect(target).to.haveOwnProperty('Date');
      });

      it('should be the correct date', () => {
        expect(target.Date.toString()).to.equal('20170206');
      });
    });
  });
});

describe('RelativeStrength Calcs - getIndexByDates realtime', () => {
  
  it('should return an object with two properties, which are quotes from the right day', () => {
    
    let endQuote = seriesRealtime[2];
    let startQuote = seriesRealtime[4];
    let period = '1d'; //realtime
    let index = mockIndexRealtimeByDate;
    const res = getIndexByDates(startQuote, endQuote, index);
  
    
    expect(typeof res).to.equal('object');
    expect(res).to.haveOwnProperty('startQuoteIndex');
    expect(res).to.haveOwnProperty('endQuoteIndex');

    describe('startQuoteIndex', () => {
      const target = res.startQuoteIndex;
      it('should be an index quote', () => {
        expect(target).to.haveOwnProperty('close');
        expect(target).to.haveOwnProperty('Timestamp');
      });

      it('should be the correct date', () => {
        expect(target.Timestamp.toString()).to.equal('1487001839');
      });
    });

    describe('endQuoteIndex', () => {
      const target = res.endQuoteIndex;
      it('should be an index quote', () => {
        expect(target).to.haveOwnProperty('close');
        expect(target).to.haveOwnProperty('Timestamp');
      });

      it('should be the correct date', () => {
        expect(target.Timestamp.toString()).to.equal('1487001660');
      });
    });
  });
});

describe('RSI Calcs - rsiList', () => {
  
  it('should return an empty array if the series length (the input) is less than 7 even if days is only 2', () => {
    const res = new RSCalcs(seriesRealtime).rsiList(2);
    expect(Array.isArray(res)).to.equal(true);
    expect(res).to.deep.equal([]);
  });

  it('should return a length equal to the length of closes', () => {
    const long = [...seriesRealtime, ...seriesRealtime].slice(4);
    const res = new RSCalcs(long).rsiList(2);
    expect(Array.isArray(res)).to.equal(true);
    expect(res.length).to.equal(8);
  });

  it('should work with 7', () => {
    const long = [...seriesRealtime, ...seriesRealtime].slice(5);
    const res = new RSCalcs(long).rsiList(2);
    expect(Array.isArray(res)).to.equal(true);
    expect(res.length).to.equal(7);
  });

  // https://docs.google.com/spreadsheets/d/1lDqCxP4YRuFlFDQ3g2DzaBAyDcbUyddVKY7uOXbULE4/edit#gid=1561149580
  it('should return similar results as the example spreadsheet link above', () => {
    // oldest to newest. Just like our quotes
    const long = [  
      {date: '14-Dec-09', close: 44.34},
      {date: '15-Dec-09', close: 44.09},
      {date: '16-Dec-09', close: 44.15},
      {date: '17-Dec-09', close: 43.61},
      {date: '18-Dec-09', close: 44.33},
      {date: '21-Dec-09', close: 44.83},
      {date: '22-Dec-09', close: 45.10},
      {date: '23-Dec-09', close: 45.42},
      {date: '24-Dec-09', close: 45.84},
      {date: '28-Dec-09', close: 46.08},
      {date: '29-Dec-09', close: 45.89},
      {date: '30-Dec-09', close: 46.03},
      {date: '31-Dec-09', close: 45.61},
      {date: '4-Jan-10', close: 46.28},
      {date: '5-Jan-10', close: 46.28},
      {date: '6-Jan-10', close: 46.00},
      {date: '7-Jan-10', close: 46.03},
      {date: '8-Jan-10', close: 46.41},
      {date: '11-Jan-10', close: 46.22},
      {date: '12-Jan-10', close: 45.64},
      {date: '13-Jan-10', close: 46.21},
      {date: '14-Jan-10', close: 46.25},
      {date: '15-Jan-10', close: 45.71},
      {date: '19-Jan-10', close: 46.45},
      {date: '20-Jan-10', close: 45.78},
      {date: '21-Jan-10', close: 45.35},
      {date: '22-Jan-10', close: 44.03},
      {date: '25-Jan-10', close: 44.18},
      {date: '26-Jan-10', close: 44.22},
      {date: '27-Jan-10', close: 44.57},
      {date: '28-Jan-10', close: 43.42},
      {date: '29-Jan-10', close: 42.66},
      {date: '1-Feb-10', close: 43.13}
    ]
    const res = new RSCalcs(long).rsiList(14);
    expect(res.length).to.equal(33);
    expect(res[res.length - 1]).to.equal(37.79);
    expect(res[14]).to.equal(70.46);
  });
});
