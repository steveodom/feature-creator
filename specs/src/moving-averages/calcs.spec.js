import {expect} from 'chai';
import MACalcs from './../../../src/moving-averages/calcs';

import {seriesDaily} from './../relative-strength/sampleQuotes';
const data = [
  {close: 0, date: '060117'},
  {close: 1, date: '060217'},
  {close: 2, date: '060317'},
  {close: 3, date: '060417'},
  {close: 4, date: '060517'},
  {close: 5, date: '060617'},
  {close: 6, date: '060717'},
  {close: 7, date: '060817'},
  {close: 8, date: '060917'},
  {close: 9, date: '061017'},
  {close: 10, date: '061117'},
  {close: 11, date: '061217'}
];

describe('Moving Average Calcs - rolling structure', () => {
  let ma = new MACalcs(data);
  let res = ma.rolling(2);

  it('should return an array of objects', () => {
    expect(Array.isArray(res)).to.equal(false);
    expect(typeof res).to.equal('object');
    expect(res).to.haveOwnProperty('avg');
    expect(res).to.haveOwnProperty('bb');
  });

  describe('bb-', () => {
    it('should have upper and lower properties', () => {
      const target = res.bb[0];
      expect(target).to.haveOwnProperty('upper');
      expect(target).to.haveOwnProperty('lower');
    });

  });

  describe('avg-', () => {
    it('should return an array', () => {
      const target = res.avg;
      expect(Array.isArray(target));
    });
  });
  
});


describe('Moving Average Calcs - calcAvgForWindow', () => {
  let ma = new MACalcs(data);
  let res;
  it('should return 0 if not history ', () => {
    res = ma.calcAvgForWindow(0, 2);
    expect(res.avg).to.equal(0);
  });

  it('should return avg of first two ', () => {
    res = ma.calcAvgForWindow(2, 4);
    //  start at 2 pos and goes back.
    // [ 0, 1 ]
    expect(res.avg).to.equal(0.5);
  });

  it('should return correct avg', () => {
    res = ma.calcAvgForWindow(6, 2);
    //  start at 6 pos and goes back.
    // [ 5, 4 ]
    expect(res.avg).to.equal(4.5);
  });

  it('should return correct avg ', () => {
    res = ma.calcAvgForWindow(6, 4);
    //  start at 2 pos and goes back.
    // [ 5, 4, 3, 2 ]
    expect(res.avg).to.equal(3.5);
  });
});


describe('MovingAverage Calcs - rolling 2 periods', () => {
  let ma = new MACalcs(data);
  let res = ma.rolling(2);
  const {avg, bb} = res;
  console.log(avg, 'avg');
  it('should return avg and bb objects', () => {  
    expect(res).to.haveOwnProperty('avg');
    expect(res).to.haveOwnProperty('bb');  
  });

  it('should start with 0 since 0 is the first number and there is no history', () => {
    expect(avg[0]).to.equal(0);
  });

  it('should the average of the first 2 numbers', () => {
    expect(avg[1]).to.equal(0.5);
  });

  it('should average the last 2 numbers', () => {
    expect(avg[avg.length - 1]).to.equal(10.5);
  });
});
  
describe('MovingAverage Calcs - rolling 4 periods', () => {
  let ma = new MACalcs(data);
  let res = ma.rolling(4);
  const {avg, bb} = res;
  
  it('should start with 0 since 0 is the first number and there is no history', () => {
    expect(avg[0]).to.equal(0);
  });

  it('should average of the first 4 numbers', () => {
    expect(avg[3]).to.equal(1.5);
  });

  it('should average of the 1,2,3,4 numbers', () => {
    expect(avg[4]).to.equal(2.5);
  });

  it('should average the last 4 numbers', () => {
    expect(avg[avg.length - 1]).to.equal(9.5);
  });
});


describe('MA Calcs - calcTrueRange', () => {
  let ma = new MACalcs(seriesDaily);
  
  // calcs: a: 129.19 - 128.16 = 1.03
  // calcs b: 129.19 - 128.53 = 0.66
  // calcs c 129.08 - 128.53 = 0.55;
  it('should return the right number', () => {
    const n = 1;
    let res = ma.calcTrueRange(seriesDaily[n], n);
    expect(typeof res).to.equal('number');
    expect(res).to.equal(1.03);
  });
  
  it('should return todays range if there is not previous quote', () => {
    const n = 0;
    let res = ma.calcTrueRange(seriesDaily[n], n);
    expect(typeof res).to.equal('number');
    expect(res).to.equal(1.61);
  });
});


describe('MA Calcs - trueRange', () => {
  let ma = new MACalcs(seriesDaily);

  it('should return an array', () => {
    const res = ma.trueRange();
    expect(Array.isArray(res)).to.be.true;

    describe('containing true range numbers', () => {
      it('should be the right range', () => {
        const target = res[0];
        expect(typeof target).to.equal('number');
        expect(target).to.equal(1.61);
      });

      it('should be the right range', () => {
        const target = res[1];
        expect(typeof target).to.equal('number');
        expect(target).to.equal(1.03);
      });
    });
  });
});

describe('MA Calcs - exponential', () => {
  let ma = new MACalcs(seriesDaily);

  it('should return an array', () => {
    const res = ma.exponential(2)
    expect(Array.isArray(res)).to.be.true;
  });
});

describe('MA Calcs - calcN', () => {
  const ma = new MACalcs(seriesDaily);
  const res = ma.calcN(1.63, 1.315, 2);
  
  it('should return a number', () => {
    expect(typeof res).to.equal('number');  
    expect(res).to.equal(1.4725);
  });
});


describe('MA Calcs - avgTrueRange', () => {
  const ma = new MACalcs(seriesDaily)
  const trs = ma.trueRange();
  const res = ma.avgTrueRange(trs, 2);
  console.log(trs, 'trueRange', res, 'avgTrueRange');
  it('should return an array', () => {
    expect(Array.isArray(res)).to.be.true;  
  });
});



