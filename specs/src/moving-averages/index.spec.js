import {expect} from 'chai';
import MovingAverage from './../../../src/moving-averages';

import {defaultSeries, defaultRecipe} from './../../../src/sample-data';


describe('MovingAverage - init', () => {

  it('should add methods to each recipe', () => {
    const b = new MovingAverage(defaultSeries, defaultRecipe);
    b.init();
    const res = b.recipe.movingAverage[0];
    expect(res).to.have.property('methods');
  });
});


describe('MovingAverage - build', () => {

  it('should return an object with keys from the basic recipe', () => {
    const b = new MovingAverage(defaultSeries, defaultRecipe);
    b.init();

    let res = b.build(defaultSeries[0], 0);
    expect(typeof res).to.equal('object');
    expect(res).to.have.property('mar-movingAverage-5');
    expect(res).to.have.property('mar-closeAboveAvg-5');
    expect(res).to.have.property('mar-daysAgoChange-5');
  });
});
