import {expect} from 'chai';
import MovingAverageMethods from './../../../src/moving-averages/methods';
import MACalcs from './../../../src/moving-averages/calcs';
import {mockIndexRealtimeByDate} from './../relative-strength/sampleQuotes';

const series = [
  {
    Date: 20160805,
    close: 782.22,
    high: 783.04,
    low: 772.34,
    open: 773.78,
    volume: 1801200
  },
  {
    Date: 20160808,
    close: 781.76,
    high: 782.63,
    low: 778.091,
    open: 782,
    volume: 1107900
  },
  {
    Date: 20160809,
    close: 784.26,
    high: 788.94,
    low: 780.57,
    open: 781.1,
    volume: 1318900
  },
  {
    Date: 20160810,
    close: 785.26,
    high: 789.94,
    low: 780.00,
    open: 784.1,
    volume: 1318900
  },
  {
    Date: 20160811,
    close: 786.26,
    high: 787.94,
    low: 783.00,
    open: 785.1,
    volume: 1318900
  }
]
const calcs = new MACalcs(series);

describe('MovingAverageMethods - movingAverage', () => {

  it('should return a number', () => {
    const recipe = {feature: 'movingAverage', name: 'ma5', 'period': 3, type: 'rolling'};
    let averages = calcs.rolling(recipe.period);
    let b = new MovingAverageMethods(series, averages);
    let quote = series[4];

    let res = b.movingAverage(quote, 4);
    expect(typeof res).to.equal('number');
  });

  it('should equal the average of the closes for the last n days where n is defined by the recipe period', () => {
    const recipe = {feature: 'movingAverage', name: 'ma5', 'period': 3, type: 'rolling'};

    let averages = calcs.rolling(recipe.period);
    let b = new MovingAverageMethods(series, averages);
    let quote = series[4];

    let res = b.movingAverage(quote, 4);
    expect(res).to.equal(785.26);
  });
});


describe('MovingAverageMethods - closeAboveAverage', () => {
  it('should return whether the close is above the n period rolling average', () => {
    const recipe = {feature: 'movingAverage', name: 'ma5', 'period': 3, type: 'rolling'};

    let averages = calcs.rolling(recipe.period);
    let b = new MovingAverageMethods(series, averages);
    let quote = series[4];

    let res = b.closeAboveAvg(quote, 4);
    expect(res).to.equal(true);
  });
});

describe('MovingAverageMethods - brokeLower', () => {
  it('should return whether the low broke the lowerDeviation ', () => {
    const recipe = {feature: 'brokeLower', name: 'brokeUpper', 'period': 3, type: 'rolling'};

    let averages = calcs.rolling(recipe.period);
    let b = new MovingAverageMethods(series, averages);
    let quote = series[4];

    let res = b.brokeLower(quote, 4);
    expect(res).to.equal(true);
  });
});


describe('MovingAverageMethods - upperDeviation', () => {
  it('should return a number', () => {
    const recipe = {feature: 'upperDeviation', name: 'upperDeviation', 'period': 3, type: 'rolling'};

    let averages = calcs.rolling(recipe.period);
    let b = new MovingAverageMethods(series, averages);
    let quote = series[4];

    let res = b.upperDeviation(quote, 4);
    expect(typeof res).to.equal('number');
  });
});

describe('MovingAverageMethods - indexDaysAgoChange', () => {
  it('should return a number', () => {
    const recipe = {feature: 'indexDaysAgoChange', name: 'upperDeviation', 'period': 3, type: 'rolling'};
    let averages = calcs.rolling(recipe.period);
    let b = new MovingAverageMethods(series, averages, mockIndexRealtimeByDate);
    let quote = series[4];

    let res = b.indexDaysAgoChange(quote, 4, {daysAgo: recipe.period}); //4, 
    console.log(res, 'change');
    expect(typeof res).to.equal('number');
  });
});
