import {expect} from 'chai';

import {removeProperties} from './../../../src/utils/helpers';

describe('Helpers - removeProperties', () => {
  it('should return the quote object', () => {
    const quote = {
      Date: 20160805,
      close: 782.22,
      high: 783.04,
      low: 772.34,
      open: 773.78,
      volume: 1801200
    };
    const remove = ['close', 'high', 'low', 'Date'];
    const res = removeProperties(quote, remove);
    expect(typeof res).to.equal('object');
    expect(res).to.not.have.property('close');
  });
});