import {expect} from 'chai';
import QuoteGroup, {featureChartObject, chartableFeatures, chartableFeaturesByGroup} from './../../../src/utils/meta';

const sampleQuote = {
  Date: '01-12-2017-09:35AM--05:00',
  Timestamp: 1484231640,
  'basic-closeAtHigh': false,
  'basic-closeAtLow': false,
  'basic-closeRangePct': 0.65,
  'basic-intradayIndex': 0,
  'basic-ticksToEndOfDay': 78,
  change: false,
  close: 119.075,
  high: 119.19,
  low: 118.86,
  'mar-brokeLower-11': false,
  'mar-brokeLower-70': false,
  'mar-brokeLower-200': false,
  'mar-brokeUpper-11': false,
  'mar-brokeUpper-70': false,
  'mar-brokeUpper-200': false,
  'mar-closeAboveAvg-11': false,
  'mar-closeAboveAvg-70': false,
  'mar-closeAboveAvg-200': false,
  'mar-daysAgoChange-11': 0,
  'mar-daysAgoChange-70': 0,
  'mar-lowerDeviation-59': 118.86,
  'mar-lowerDeviation-70': 118.86,
  'mar-lowerDeviation-200': 118.86,
  'mar-movingAverage-11': 119.08,
  'mar-movingAverage-70': 119.08,
  'mar-movingAverage-200': 119.08,
  'mar-upperDeviation-11': 119.19,
  'mar-upperDeviation-70': 119.19,
  'mar-upperDeviation-200': 119.19,
  'mx-5': Array[1],
  'mx-35': Array[1],
  'mx-70': Array[1],
  open: 118.895,
  roundedDate: '01-12-2017-0:35AM--0:00',
  'sr-nearLevel-50-12': true,
  'sr-nearLevel-50-50': true,
  'sr-nearLevel-50-90': true,
  'sr-nearLevel-100-12': true,
  'sr-nearLevel-100-50': true,
  'sr-nearLevel-100-90': true,
  'sr-nearLevel-236-12': true,
  'sr-nearLevel-236-50': true,
  'sr-nearLevel-236-90': true,
  'sr-nearLevel-382-12': true,
  'sr-nearLevel-382-50': true,
  'sr-nearLevel-382-90': true,
  'sr-nearLevel-618-12': true,
  'sr-nearLevel-618-50': true,
  'sr-nearLevel-618-90': true,
  ticker: 'aapl',
  volume: 2442100
}

describe('Utils Meta - QuoteGroup', () => {
  const res = new QuoteGroup(sampleQuote);
  it('should have labels populated', () => {
    expect(res).to.have.property('labels');
    expect(Array.isArray(res.labels)).to.equal(true);
    expect(res.labels.includes('close')).to.equal(true);
    expect(res.labels.includes('basic-closeAtHigh')).to.equal(true);
    expect(res.labels.includes('sr-nearLevel-618-90')).to.equal(true);
  });
});

describe('Utils Meta - QuoteGroup find', () => {
  const res = new QuoteGroup(sampleQuote);
  
  it('should find the right label', () => {
    const target = res.find('basic');
    expect(res.labels.includes('basic-closeAtHigh')).to.equal(true);
    expect(res.labels.includes('basic-closeAtLow')).to.equal(true);
    expect(res.labels.includes('basic-closeRangePct')).to.equal(true);
  });
});



describe('Utils Meta - QuoteGroup build', () => {
  const res = new QuoteGroup(sampleQuote).build();
  it('should return an array', () => {
    expect(Array.isArray(res)).to.equal(true);
  });

  it('should contain close', () => {
    const target = res.find( (meta) => (meta.label === 'Close') );
    expect(target.key).to.equal('close');
  });

  it('should contain basic-closeAtLow', () => {
    const target = res.find( (meta) => (meta.key === 'basic-closeAtLow') );
    expect(target).to.not.be.undefined;
  });

  it('should contain basic-closeAtLow', () => {
    const target = res.find( (meta) => (meta.key === 'mar-closeAboveAvg-11') );
    expect(target).to.not.be.undefined;
  });
});

describe('Utils Meta - QuoteGroup buildAndGroup', () => {
  const res = new QuoteGroup(sampleQuote).buildAndGroup();
  it('should return an object', () => {
    expect(typeof res).to.equal('object');
  });

  it('should have a Price Data property', () => {
    expect(res).to.haveOwnProperty('Price Data');
  });

  describe('Price Data and other properties', () => {
    const target = res['Price Data'];
    it('should be an object', () => {
      expect(typeof target).to.equal('object');
    });

    it('should have a datasets property that is an array', () => {
      expect(target).to.haveOwnProperty('datasets');
      expect(Array.isArray(target['datasets'])).to.be.true;
    })
  });
});

// describe('Utils Meta - featureChartObject', () => {
  
//   it('should return an object with visible, axis, and label properties', () => {
//     const name = 'close';
//     const res = featureChartObject(name);
//     expect(typeof res).to.equal('object');
//     expect(res).to.have.property('hidden');
//     expect(res).to.have.property('axis');
//     expect(res).to.have.property('label');
//   });

//   it('should return null if base name is not found', () => {
//     const name = 'mx5';
//     const res = featureChartObject(name);
//     expect(res).to.equal(null);
//   });

//   it('should return null if base name found but feature not', () => {
//     const name = 'base-noFeature';
//     const res = featureChartObject(name);
//     expect(res).to.equal(null);
//   });
// });

// describe('Utils Meta - chartableFeatures', () => {
//   const quote = {Date: "11072016",
//     above5Avg: false,
//     change: false,
//     close: 111.06,
//     closeAtHigh: false,
//     closeAtLow: false,
//     closeRangePct: 0.67,
//     logClose: 4.71,
//     logHigh: 4.716,
//     logLow: 4.698,
//     logOpen: 4.703,
//     volume: 3000000
//   }
//   it('should return an array', () => { 
//     const res = chartableFeatures(quote);
//     expect(Array.isArray(res)).to.equal(true);
//   });

//   it('should have two elements, high and close, for this example', () => { 
//     const res = chartableFeatures(quote);
//     expect(res.length).to.equal(2);
//     expect(res[0].label).to.equal('Close');
//     expect(res[1].label).to.equal('Volume');
//   });

//   it('should not have any null members', () => { 
//     const res = chartableFeatures(quote);
//     expect(res.length).to.equal(2);
//   });
// });

// describe('Utils Meta - chartableFeaturesByGroup', () => {
//   const quote = {Date: "11072016",
//     above5Avg: false,
//     change: false,
//     close: 111.06,
//     closeAtHigh: false,
//     closeAtLow: false,
//     closeRangePct: 0.67,
//     'basic-logClose': 4.71,
//     'basic-logHigh': 4.716,
//     'basic-logLow': 4.698,
//     'basic-logOpen': 4.703,
//     volume: 3000000
//   }
//   it('should return an object', () => { 
//     const res = chartableFeaturesByGroup(quote);
//     expect(typeof res).to.equal('object');
//   });

//   it('should return an object with Price Date property with an object as value', () => { 
//     const res = chartableFeaturesByGroup(quote);
//     expect(res).to.have.property('Price Data');
//     expect(typeof res['Price Data']).to.equal('object');
//     expect(res['Price Data']).to.have.property('datasets');
//   });

//   it('should return an object with Log Date property with an array value', () => { 
//     const res = chartableFeaturesByGroup(quote);
//     expect(res).to.have.property('Log Data');
//     expect(typeof res['Log Data']).to.equal('object');
//   });

// });