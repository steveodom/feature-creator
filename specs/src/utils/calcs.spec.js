import {expect} from 'chai';

import {
  nextDayChange,
  determineStart,
  arrayWindow,
  arrayStartPosition,
  arrayAdd,
  arrayAvg,
  arrayQuartiles,
  arrayTicks,
  arrayChunks,
  binnedNum,
  arrayDeviation,
  targetArray,
  targetHighsAndLows,
  isWithin
} from './../../../src/utils/calcs';

describe('Calcs', () => {
  it('nextDayChange', function() {
    let res = nextDayChange({close: 1}, {close: 2});
    expect(res).to.equal(100);

    res = nextDayChange({close: 2}, {close: 1});
    expect(res).to.equal(-50);

    res = nextDayChange({close: 2}, {close: 3});
    expect(res).to.equal(50);
  });


  it('determineStart', function() {
    // should start at 0;
    let {start, l} = determineStart(2, 4);
    expect(start).to.equal(0);
    expect(l).to.equal(2);
  });


  it('determineStart - starting with 0', function() {
    let { start, l } = determineStart(0, 4);
    expect(start).to.equal(0);
    expect(l).to.equal(0);
  });

  it('arrayStartPosition', function() {
    let start = 2;
    let res = arrayStartPosition([0,1,2,3,4], start);
    expect(res).to.deep.equal([2,3,4], '-- should drop from the 2 position');

    start = 4;
    res = arrayStartPosition([10,11,22,13,44], start);

    expect(res).to.deep.equal([44], '-- should drop from the 4 position');
  });

  it('arrayWindow', function() {
    let ary = [0,1,2,3,4,5,6,7,8,9,10,11];

    let res = arrayWindow(ary, 0, 2);
    expect(res).to.deep.equal([0,1], '-- should start from 0 pos and return 2 elements');

    res = arrayWindow(ary, 1, 2);
    expect(res).to.deep.equal([1,2], '-- should start from 1 pos and return 2 elements');

    res = arrayWindow(ary, 2, 3);
    expect(res).to.deep.equal([2,3,4], '-- should start from 2 pos and return 3 elements');
  });

  it('add', function() {
    let x = [1,2,3];
    let res = arrayAdd(x);
    expect(res).to.equal(6);

    x = [];
    res = arrayAdd(x);
    expect(res).to.equal(0);
  });

  it('arrayAvg', function() {
    let res = arrayAvg([5, 10]);
    expect(res).to.equal(7.5);
  });

  it('arrayQuartiles', function() {
    let res = arrayQuartiles([ 1, 2, 3, 4, 5 ]);
    expect(typeof res).to.equal('object');

    expect(res.q1).to.equal(2);

    res = arrayQuartiles([ 0, 25, 50, 75, 100 ]);
    expect(res.q2).to.equal(50);
    expect(res.q3).to.equal(75);
  });



  it('arrayTicks', function() {
    // not sure why these return different length arrays
    let res = arrayTicks([ 1, 2, 3, 4, 5 ]);
    expect(Array.isArray(res)).to.be.ok;
    expect(res).to.deep.equal([ 1, 2, 3, 4, 5 ])

    res = arrayTicks([ 0, 25, 50, 75, 100 ]);
    expect(res).to.deep.equal([ 0, 50, 100 ])
    expect(res[1]).to.equal(50);
    expect(res[2]).to.equal(100);
  });


  describe('arrayChunks', () => {
    let ary = [ 1, 2, 3, 4, 5 ];
    let res = arrayChunks(ary, 3, true);

    it('should return an array', () => {
      expect(Array.isArray(res)).to.be.ok;
    });

    it('should break the array into the right number of pieces', () => {
      expect(res.length).to.equal(3);

      expect(res[0]).to.deep.equal([1,2]);
      expect(res[2]).to.deep.equal([5]);
    });
  });

  describe('binnedNum', () => {

    it('should return 0 for smallest number', () => {
      let res = binnedNum(1725918);
      expect(res).to.equal(0);
    });

    it('should return 4 for largest number', () => {
      let res = binnedNum(15000000);
      expect(res).to.equal(4);
    });

    it('should return 2 for mid number', () => {
      let res = binnedNum(6180224);
      expect(res).to.equal(2);
    });

  });


  describe('arrayDeviation', () => {

    it('should return deviation', () => {
      let ary = [0,1,2,3,4,5,6,7,8,9,10,11];
      let res = arrayDeviation(ary);
      expect(res).to.equal(3.605551275463989);
    });

    it('should return 0 deviation if all the same', () => {
      let ary = [1,1,1,1,1,1];
      let res = arrayDeviation(ary);
      expect(res).to.equal(0);
    });

  });

});

describe('Utils Calcs - targetArray', () => {
  let series = [
    {close: 55, high: 60, low: 50},
    {close: 55, high: 60, low: 50},
    {close: 55, high: 60, low: 50},
    {close: 55, high: 60, low: 50},
    {close: 55, high: 60, low: 50},
    {close: 50, high: 52, low: 40},
    {close: 48, high: 50, low: 43},
    {close: 55, high: 60, low: 50},
    {close: 55, high: 60, low: 50},
    {close: 55, high: 60, low: 50},
    {close: 55, high: 60, low: 50},
    {close: 55, high: 60, low: 50},
  ];

  it('should return an array', () => {
    const res = targetArray(6, 5, series);
    expect(Array.isArray(res)).to.equal(true);
  });

  it('should return an array of the right length', () => {
    const res = targetArray(6, 5, series);
    expect(res.length).to.equal(5);
  });
});

describe('Utils Calcs - targetHighsAndLows', () => {
  const series = [
    {close: 55, high: 60, low: 50},
    {close: 55, high: 60, low: 50},
    {close: 55, high: 60, low: 50},
    {close: 55, high: 60, low: 50},
    {close: 55, high: 60, low: 50},
    {close: 50, high: 52, low: 40},
    {close: 48, high: 50, low: 43},
    {close: 55, high: 60, low: 50},
    {close: 55, high: 60, low: 50},
    {close: 55, high: 61, low: 50},
    {close: 55, high: 60, low: 50},
    {close: 55, high: 60, low: 50},
  ];

  it('should return an object with high and low properties', () => {
    const target = targetArray(11, 11, series);
    const res = targetHighsAndLows(target);

    expect(typeof res).to.equal('object');
    expect(res).to.have.property('high');
    expect(res).to.have.property('low');
  });

  it('should return the low and high', () => {
    const target = targetArray(11, 11, series);
    const res = targetHighsAndLows(target);
    expect(res.high).to.equal(61);
    expect(res.low).to.equal(40);
  });
});

describe('Utils Calcs - isWithin', () => {
  it('should return a boolean', () => {
    const res = isWithin(9, 10, 0.05);
    expect(typeof res).to.equal('boolean'); 
  });

  it('should return false if subject is not Within the threshold of the target', () => {
    const res = isWithin(9, 10, 0.05);
    expect(res).to.equal(false); 
  });

  it('should return true if subject isWithin the threshold of the target', () => {
    const res = isWithin(9, 10, 0.11);
    expect(res).to.equal(true); 
  });

  it('should return true if subject equal to the threshold of the target', () => {
    const res = isWithin(9, 10, 0.10);
    expect(res).to.equal(true); 
  });
});
