import {expect} from 'chai';
import {
  intradayIndex, 
  periodsInTradingDay, 
  convertTimestampToNYHour,
  ticksToEndOfDay,
  startOfTrading,
  endOfTrading
} from './../../../src/basic/calcs';
import moment from 'moment';

describe('Basic Calcs - intraday index', () => {

  it('should return a number', () => {
    const timestamp = '1480872143';
    const res = intradayIndex(timestamp, 5);
    expect(typeof res).to.equal('number');
  });

  it('should return a 1 if 8:35 and 5 minute increment', () => {
    const timestamp = '1446388500'; //11/1/2016 8:35am CST
    const res = intradayIndex(timestamp, 5);
    expect(res).to.equal(1);
  });

  it('should return a 1 if 8:37 and 5 minute increment', () => {
    const timestamp = '1446388620'; //12/1/2016 8:37am CST
    const res = intradayIndex(timestamp, 5);
    expect(res).to.equal(1);
  });

  it('should return a 1 if 8:38 and 5 minute increment', () => {
    const timestamp = '1446388680'; //12/1/2016 8:38am CST
    const res = intradayIndex(timestamp, 5);
    expect(res).to.equal(1);
  });

  it('should return a 2 if 8:40 and 5 minute increment', () => {
    const timestamp = '1446388800'; //12/1/2016 8:40am CST
    const res = intradayIndex(timestamp, 5);
    expect(res).to.equal(2);
  });

  it('should return a 0 if 8:38 and 10 minute increment', () => {
    const timestamp = '1446388680'; //12/1/2016 8:38am CST
    const res = intradayIndex(timestamp, 10);
    expect(res).to.equal(0);
  });

  it('should return a 1 if 8:40 and 10 minute increment', () => {
    const timestamp = '1446388800'; //12/1/2016 8:40am CST
    const res = intradayIndex(timestamp, 10);
    expect(res).to.equal(1);
  });

  it('should return a 16 if 9:50 and 5 minute increment', () => {
    const timestamp = '1480953000'; //12/5/2016 9:50am CST
    const res = intradayIndex(timestamp, 5);
    expect(res).to.equal(16);
  });
});

describe('Basic Calcs - ticksToEndOfDay', () => {

  it('should return a number', () => {
    const timestamp = '1480872143';
    const res = ticksToEndOfDay(timestamp, 5);
    expect(typeof res).to.equal('number');
  });

  it('should return a 1 if 2:55 and 5 minute increment', () => {
    const timestamp = '1446411300';
    const res = ticksToEndOfDay(timestamp, 5);
    expect(res).to.equal(1);
  });

  it('should return a 0 if 2:55 and 10 minute increment', () => {
    const timestamp = '1446411300';
    const res = ticksToEndOfDay(timestamp, 10);
    expect(res).to.equal(1);
  });

  it('should return a 78 if 8:30am and 5 minute increment', () => {
    const timestamp = '1446388200';
    const res = ticksToEndOfDay(timestamp, 5);
    expect(res).to.equal(78);
  });

  it('should return a 77 if 8:36am and 5 minute increment', () => {
    const timestamp = '1446388560';
    const res = ticksToEndOfDay(timestamp, 5);
    expect(res).to.equal(77);
  });
});