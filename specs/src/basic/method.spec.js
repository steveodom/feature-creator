import {expect} from 'chai';
import BasicMethods from './../../../src/basic/methods';
import {nextDayChange} from './../../../src/utils/calcs';

describe('BasicMethods - logClose', () => {

  it('should return a number', () => {
    let series = [
      {close: 5.5, open: 5, high: 6, low: 5}
    ];
    let res = new BasicMethods(series).logClose(series[0]);
    expect(typeof res).to.equal('number');
    expect(res).to.equal(1.705);
  });

  it('should return a small number, even with bigger numbers', () => {
    let series = [
      {close: 95.5, open: 95, high: 96, low: 95}
    ];
    let res = new BasicMethods(series).logClose(series[0]);
    expect(res).to.equal(4.559);
  });

  it('should return a small number, even with bigger numbers', () => {
    let series = [
      {close: 15.5, open: 15, high: 16, low: 15}
    ];
    let res = new BasicMethods(series).logClose(series[0]);
    expect(res).to.equal(2.741);
  });

  it('should not accurately reflect daily changes', () => {
    // doesn't accuractly reflect changes. Run models and see how this works'
    let series = [{close: 8}];
    let series1 = [{close: 10}];

    let res1 = new BasicMethods(series).logClose(series[0]);
    let res2 = new BasicMethods(series).logClose(series1[0]);

    let change = nextDayChange(series1[0], series[0]);
    let changeLog = nextDayChange({close: res2}, {close: res1});
    expect(change).to.not.equal(changeLog);
  });
});

describe('BasicMethods - closeRangePct', () => {

  it('should return a percentage', () => {
    let series = [
      {close: 5.5, open: 5, high: 6, low: 5}
    ];
    let res = new BasicMethods(series).closeRangePct(series[0]);
    expect(typeof res).to.equal('number');
    expect(res).to.equal(0.5);
  });

  it('should return a percentage2', () => {
    let series = [
      {close: 5.5, open: 5, high: 10, low: 5}
    ];
    let res = new BasicMethods(series).closeRangePct(series[0]);
    expect(res).to.equal(0.1);
  });
});


describe('BasicMethods - intradayIndex', () => {

  it('should return a number', () => {
    const Timestamp = '1446388500'; //12/1/2016 8:35am CST
    let series = [
      {Timestamp, close: 5.5, open: 5, high: 6, low: 5}
    ];
    let res = new BasicMethods(series).intradayIndex(series[0]);
    expect(typeof res).to.equal('number');
    expect(res).to.equal(1);
  });
});
