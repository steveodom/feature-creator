import {expect} from 'chai';
import Basic from './../../../src/basic';

import {defaultSeries, defaultRecipe} from './../../../src/sample-data';

describe('basic', () => {

  it('should return an object with keys from the basic recipe', () => {
    const b = new Basic(defaultSeries, defaultRecipe);

    let res = b.build(defaultSeries[0], 0);
    expect(typeof res).to.equal('object');
    expect(res).to.have.property('basic-closeAtLow');
    expect(res).to.have.property('basic-closeAtHigh');
  });
});
