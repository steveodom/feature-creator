import {expect} from 'chai';
import SRMethods from './../../../src/support-resistance/methods';
import SRCalcs from './../../../src/support-resistance/calcs';

const series = [
  {
    Date: 20160805,
    close: 782.22,
    high: 783.04,
    low: 772.34,
    open: 773.78,
    volume: 1801200
  },
  {
    Date: 20160808,
    close: 781.76,
    high: 782.63,
    low: 778.091,
    open: 782,
    volume: 1107900
  },
  {
    Date: 20160809,
    close: 784.26,
    high: 788.94,
    low: 780.57,
    open: 781.1,
    volume: 1318900
  },
  {
    Date: 20160810,
    close: 785.26,
    high: 789.94,
    low: 780.00,
    open: 784.1,
    volume: 1318900
  },
  {
    Date: 20160811,
    close: 786.26,
    high: 787.94,
    low: 783.00,
    open: 785.1,
    volume: 1318900
  }
]
const sr = new SRCalcs(series);

describe('SRMethods - nearLevel', () => {

  it('should return a boolean', () => {
    const recipe = {feature: 'nearLevel', name: 'nearLevel236', 'period': 2, level: 236 };
    let averages = sr.fibonacci(recipe.period);
    let b = new SRMethods(series, averages);
    let quote = series[4];

    let res = b.nearLevel(quote, 4, recipe);
    expect(typeof res).to.equal('boolean');
    expect(res).to.equal(false);
  });

  it('should return true if the quote passed in is near the fib level', () => {
    const recipe = {feature: 'nearLevel', name: 'nearLevel236', 'period': 2, level: 236 };
    let averages = sr.fibonacci(recipe.period);
    let b = new SRMethods(series, averages);
    let quote = {close: 782.3458400000001};
    let res = b.nearLevel(quote, 4, recipe);
    expect(res).to.equal(true);
  });

});


