import {expect} from 'chai';
import SRCalcs from './../../../src/support-resistance/calcs';
import {seriesRealtime} from './../relative-strength/sampleQuotes';

describe('SR Calcs - calcFibonacci', () => {
  let sr = new SRCalcs([]);

  it('shoul return an object with fibonacci levels as properties', () => {
    const res = sr.calcFibonacci(100, 0);
    expect(typeof res).to.equal('object');
    expect(res).to.have.property('100');
    expect(res).to.have.property('618');
    expect(res).to.have.property('50');
    expect(res).to.have.property('382');
    expect(res).to.have.property('236');
  });

  it('should return the right level calculations', () => {
    const res = sr.calcFibonacci(60, 50);
    
    expect(res['100']).to.equal(60);
    expect(res['50']).to.equal(55);
    expect(res['618']).to.equal(56.18);
    expect(res['236']).to.equal(52.36);
    
  });
});

describe('SR Calcs - calcFibonacciForWindow', () => {

  it('should return an object with fibonacci levels as properties', () => {
    const series = [
      {
        Date: 20160805,
        close: 782.22,
        high: 783.04,
        low: 772.34,
        open: 773.78,
        volume: 1801200
      },
      {
        Date: 20160808,
        close: 781.76,
        high: 782.63,
        low: 778.091,
        open: 782,
        volume: 1107900
      },
      {
        Date: 20160809,
        close: 784.26,
        high: 788.94,
        low: 780.57,
        open: 781.1,
        volume: 1318900
      },
      {
        Date: 20160810,
        close: 785.26,
        high: 789.94,
        low: 780.00,
        open: 784.1,
        volume: 1318900
      },
      {
        Date: 20160811,
        close: 786.26,
        high: 787.94,
        low: 783.00,
        open: 785.1,
        volume: 1318900
      }
    ]
    const sr = new SRCalcs(series);
    const res = sr.calcFibonacciForWindow(4,3);
    expect(typeof res).to.equal('object');
    expect(res).to.have.property('100');
    expect(res).to.have.property('618');
    expect(res).to.have.property('50');
    expect(res).to.have.property('382');
    expect(res).to.have.property('236');
    expect(res['50']).to.equal(784.0155);
  });
});

describe('SR Calcs - obv', () => {
  
  it('should return an array of numbers that look kind of like volume', () => {
    const series = [...seriesRealtime, ...seriesRealtime];
    const res = new SRCalcs(series).obv();
    expect(Array.isArray(res)).to.equal(true);
    expect(res.length).to.equal(series.length);
  });
});
