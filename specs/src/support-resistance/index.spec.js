import {expect} from 'chai';
import SupportResistance from './../../../src/support-resistance';

import {defaultSeries, defaultRecipe} from './../../../src/sample-data';


describe('SupportResistance - init', () => {

  it('should add methods to each recipe', () => {
    const b = new SupportResistance(defaultSeries, defaultRecipe);
    b.init();
    const res = b.recipe.supportResistance[0];
    expect(res).to.have.property('methods');
  });
});


describe('SupportResistance - build', () => {

  it('should return an object with keys from the basic recipe', () => {
    const b = new SupportResistance(defaultSeries, defaultRecipe);
    b.init();

    let res = b.build(defaultSeries[0], 0);
    expect(typeof res).to.equal('object');
    expect(res).to.haveOwnProperty('sr-nearLevel-236-20');
    expect(res).to.haveOwnProperty('sr-supportLevel-236-5');
  });
});