import nock from 'nock';
import {defaultSeries} from './../src/sample-data';
import {mockIndexRealtime} from './src/relative-strength/sampleQuotes';

export const nockEndpoint = (ticker, period, klass) => {
  let response = defaultSeries;
  if (klass === 'index') {
    response = mockIndexRealtime;
  }
  nock('http://chartapi.finance.yahoo.com')
    .get(`/instrument/1.0/${ticker}/chartdata;type=quote;range=${period}/json`)
    .reply(200, `finance_charts_json_callback( { "meta" :\n {\n}\n ,\n "series" : ${JSON.stringify(response)}\n} )`
    );
};
