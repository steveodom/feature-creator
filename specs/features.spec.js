import {expect} from 'chai';
import hydrateWithFeatures from './../src/features.js';

import {defaultSeries, defaultRecipe} from './../src/sample-data';
import {mockIndexDailyByDate} from './src/relative-strength/sampleQuotes';

describe('Features - hydrateWithFeatures', () => {
  let features = hydrateWithFeatures(defaultSeries, defaultRecipe, 'aapl', mockIndexDailyByDate);

  it('should return an array of quotes', () => {
    expect(Array.isArray(features)).to.equal(true);
    expect(features.length).to.equal(3);
  });

  it('should return elements that are object with keys from the recipe', () => {
    let p = Promise.resolve(features[0]);
    return p.then( (res) => {
      expect(typeof res).to.equal('object');
      expect(res).to.have.property('basic-closeAtLow');
      expect(res).to.have.property('basic-closeAtHigh');
    });  
  });

  it('should return elements that are object with identifying information', () => {
    let p = Promise.resolve(features[0]);
    return p.then( (res) => {
      expect(res).to.have.property('ticker');
      expect(res).to.have.property('roundedDate');
    })
  });

  it('should return elements that are object with movingAverage information', () => {
    let p = Promise.resolve(features[0]);
    return p.then( (res) => {
      expect(res).to.have.property('mar-movingAverage-5');
      expect(res).to.have.property('mar-closeAboveAvg-5');
      expect(res).to.have.property('mar-daysAgoChange-5');
    });
  });

  it('should return elements that are object with MatrixChart information', () => {
    let p = Promise.resolve(features[0]);
    return p.then( (res) => {
      expect(res).to.have.property('mxc-5');
      expect(res).to.have.property('mxv-20');
    });
  });

  it('should return elements that are objects with SupportResistance information', () => {
    let p = Promise.resolve(features[0]);
    return p.then( (res) => {
      expect(res).to.have.property('sr-nearLevel-236-20');
    });
  });

  it('should return elements that are objects with Relative Strength information', () => {
    let p = Promise.resolve(features[0]);
    return p.then( (res) => {
      expect(res).to.have.property('rsi-relativePct-5');
    });
  });

  describe('create - with invalid recipe', () => {
    it('should return an array of quotes', () => {
      const modifiedRecipe = Object.assign({}, defaultRecipe, {
        matrixChart: [
          {feature: 'chartNope', name: 'mx5', 'period': 5, type: 'closes', granularity: 8}
        ]
      });

      let res = hydrateWithFeatures(defaultSeries, modifiedRecipe);
      expect(Array.isArray(res)).to.equal(false);
      expect(typeof res).to.equal('object');
      expect(res.isValid).to.equal(false);
      expect(res.msg[0]).to.equal('Specified MC feature was not found: chartNope');
    });
  });

  describe('remove properites', () => {

    it('should return an array of quotes with properties removed', () => {
      const modifiedRecipe = Object.assign({}, defaultRecipe, {
        removeFromQuote: ['close', 'high', 'low', 'open']
      });

      const res = hydrateWithFeatures(defaultSeries, modifiedRecipe, 'aapl', mockIndexDailyByDate);
      expect(res[0]).to.not.have.property('close');
      expect(res[0]).to.not.have.property('high');
      expect(res[0]).to.not.have.property('low');
      expect(res[0]).to.not.have.property('open');
      
    });
  });

});

